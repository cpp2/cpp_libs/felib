/*!
 * \file meshClass.hpp
 * \brief Diese Klasse enthaelt alle Informationen ueber das Mesh
 */

#pragma once

#include <vector>

#include <myLogger.hpp>

#include <math_toolbox.hpp>

#include "./node.hpp"
#include "./volumeElement.hpp"
#include "./faceElement.hpp"


/*!
 * Diese Klasse enthaelt alle Informationen ueber das Mesh.
 */

class meshClass{

private:
    std::vector<node> nodelist ; /// Knotenliste
    std::vector<volumeElement> elementlist ; /// Elementliste
    std::vector<faceElement> facelist; /// face-Liste

    // sizes
    int dimension;

    int nOrder;
    int nElements;
    int nPoints;
    int nFaces;
    int nBoundaryPoints;
    int nDirichlet;
    int nNeumann;
    int nBoundaryFaces;



public:

    // Konstruktor
    meshClass():dimension(0),nOrder(0),nElements(0),nPoints(0),nFaces(0),nBoundaryPoints(0), nDirichlet(0), nNeumann(0), nBoundaryFaces(0){}


    ///Printe das gesamte Gitter mit allen darauf gespeicherten Informationen
    void print() ;

    ///Getter
    int getnDimension() const;
    ///Getter
    int getnBoundary() const;
    ///Getter
    int getnElements() const;
    ///Getter
    int getnFaces() const;
    ///Getter
    int getnNeumann() const;
    ///Getter
    int getnOrder() const;
    ///Getter
    int getnPoints() const;
    ///Getter
    int getnDirichlet() const;
    ///Getter
    int getnBoundaryFaces() const;
    ///Getter
    int getGlobalNodeIndexFromElement(int elementIndex, int localPointIndex);
    ///Getter
    node* getGlobalNodeFromElement(int elementIndex, int localPointIndex);
    ///Getter
    node* returnNodefromNodelist(int T);

    ///Getter
    volumeElement * getElement(int Index) ;
    ///Getter
    std::vector<node> * getNodelist() ;

    ///Setter
    void setDimension(int d);
    /*!
     * Setzt die x- und y-Koordinate eines bestimmten Knotens in der pointlist.
     */
    void set_point(double x_in, double y_in, int nodeNumber);
    /*!
     * Setzt den Dirichlet-Boolean eines bestimmten Knotens in der pointlist.
     */
    void set_boundary(bool dirichlet_in, int nodeNumber);
    ///fuegt zusaetzlichen Punkt zur nodelsit hinzu
    int insertNode(const node n);

    /*!
     * erstellt das Referenzelement der Dimension 1-3 fuer Ordnung 1
     */
    void defineRefElementFirstOrder(int dimension);
    /*!
     * erstellt das Referenzelement 3. Dimension fuer Ordnung 2
     */
    void defineRefElement_SecondOrder(int dimension);
    /*!
     * erstellt das Referenzelement 3. Dimension fuer Ordnung 3
     */
    void defineRefElement_ThirdOrder(int dimension);
    /*!
     * erstellt das TestMesh in der Dimension, die dem meshClass-Objekt zuvor zugewiesen wurde
     */
    void createTestMesh(const int n);

    ///guckt, ob ein face für das 2D Netz am Rand liegt
    int faceCheckX(int, int);
    ///guckt, ob ein face für das 2D Netz am Rand liegt
    int faceCheckY(int, int);

    ///Getter
    int getNeighbour(int elIndex, int localFaceIndex);
    ///Setter fuer die Transformation
    void determineTransformations();
    ///Getter
    std::vector<point*> getVecOfPointsFromElement(element *el);
    ///fuegt 2D highOrderPoints auf einem Element hinzu
    void addHighOrderPointToElement(int elementIndex, int nodeIndex, int faceIndex);

    ///Fügt bei einem Ordnung 1 mesh die high-order-points hinzu
    void addHighOrderPoints(int order);
    ///Setter
    void set_nPoints(int n);
    ///Counter
    void increase_nPoints(int i);
    ///Counter
    void increase_nElements(int j);
    ///Counter
    void increase_nBoundary(int j);

    //   ab hier die Erstellungsmethoden für das 3D Tetraeder mesh

    /*!
     * erzeugt das Wuerfelgitter fuer die Tetraeder-Nodes
     */
    void set_tetrahedralVertices(int n);
    /*!
     * holt Eckpunkte der Wuerfel ein
     */
    std::vector<int> get_cubeVertices(int i, int j, int l, int n);
    /*!
     * fuegt ein element (Tetraeder) zur elementlist hinzu
     */
    void push_Tetrahedron(int T1, int T2, int T3, int T4);

    /*!
     * fuegt fuer einen Wuerfel im Gitter die inneren Tetraeder hinzu, i, j, l: Kennung des Wuerfels im Gitter
     */
    void add_Tetrahedrons(int i, int j, int l, int mod, int n);
    /*!
     *fasst 2 Routinen zusammen: erstellt das Punktgitter mit den Tetraedern
     */
    void build_allTetrahedrons(int n);
    /*!
     * ueberprueft fuer ein Face die Rand-Eigenschaft, Integer: Nummerierung der Punkte gemaess nodelist
     */
    bool check_boundary(int T_i, int T_j, int T_l);
    /*!
     * fuegt ein Face mit den Nodes i,j,l zur Facelist hinzu
     * setze neighbour2 =-1 fuer Randfaces
     */
    void add_Face(int i, int j,  int l, int neighbour1, int neighbour2);
    /*!
     * gibt fuer ein Face im Wuerfel der Kennung (i,j,l) die Elementnummer des benachbarten Tetraders gemaeß elementlist zurück
     * T_i,T_j,T_l: Nodenummern im jeweiligen Wuerfel (Nummern 1-8)
     */
    std::vector<int> detect_faceContainers(int i, int j, int l, int n, int T_i, int T_j, int T_l);
    /*!
     * sucht fuer ein Tetraeder im Wuerfel der Kennung i,j,l die zum entprechenden Face passenden Nodenummern gemaeß nodelist
     */
    std::vector<int> detect_VerticesofElement(int i, int j, int l, int n, int T_i, int T_j, int T_l);
    /*!
     * fuegt Faces sowie Faces der inneren Tetraeder fuer die Randfaces der Wuerfel Typ2 noch hinzu
     */
    void add_special_Faces(int n);
    /*!
     * erstellt alle Face-Information
     */
    void build_all_Faces(int n);
    /*!
     * erstellt ganzes standard 3D Tetraeder Gitter
     */
    void build_completeMesh(int n);
    /*!
     * fuegt auf einer Kante eines Tetraeders zusaetzliche Nodes hinzu
     */
    std::vector<node*> compute_affineNodes(int anzahl, volumeElement &tetrahedron ,int eckpunkt1, int eckpunkt2);
    node* compute_middleNode( volumeElement &tetrahedron ,int eckpunkt1, int eckpunkt2);
    /*!
     * Methode, um die Indizes der Tetraeder in der elementlist des anliegenen cube zu berechnen
     */
    int gleich (int n, int i, int j, int l);
    /*!
     * Methode, um die Indizes der Tetraeder in der elementlist des anliegenen cube zu berechnen
     */
    int rechts (int n, int i, int j, int l);
    /*!
     * Methode, um die Indizes der Tetraeder in der elementlist des anliegenen cube zu berechnen
     */
    int unten  (int n, int i, int j, int l);
    /*!
     * Methode, um die Indizes der Tetraeder in der elementlist des anliegenen cube zu berechnen
     */
    int links  (int n, int i, int j, int l);
    /*!
     * Methode, um die Indizes der Tetraeder in der elementlist des anliegenen cube zu berechnen
     */
    int oben   (int n, int i, int j, int l);
    /*!
     * Methode, um die Indizes der Tetraeder in der elementlist des anliegenen cube zu berechnen
     */
    int vorne  (int n, int i, int j, int l);
    /*!
     * Methode, um die Indizes der Tetraeder in der elementlist des anliegenen cube zu berechnen
     */
    int hinten (int n, int i, int j, int l);
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T1_T4_T6_T7();
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T1_T2_T4_T6();
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T1_T3_T4_T7();
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T4_T6_T7_T8();
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T1_T5_T6_T7();
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T2_T3_T5_T8();
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T1_T2_T3_T5();
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T2_T3_T4_T8();
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T2_T5_T6_T8();
    /*!
     * Methode zur Bestimmung der Tetraedernummer im anliegenden cube
     */
    int T3_T5_T7_T8();
    /*!
     * Methode zur Bestimmung der Kante des besagten Tetraeders im anliegenden cube
     */
    int T1_T5_T6_T7(int P1, int P2);
    /*!
     * Methode zur Bestimmung der Kante des besagten Tetraeders im anliegenden cube
     */
    int T4_T6_T7_T8(int P1, int P2);
    /*!
     * Methode zur Bestimmung der Kante des besagten Tetraeders im anliegenden cube
     */
    int T1_T2_T4_T6(int P1, int P2);

    /*!
     * fuege neuen Punkt zu den uebergegebenen Elementen hinzu
     */
    void insert_highOrderPoint(int order, int n, int nodenumber,int el0, int edge0,  int el1, int edge1, int el2, int edge2 , int el3, int edge3, int el4, int edge4 );
    /*!
     * dieselbe Methode fuer 6 anliegende Tetraeder
     */
    void insert_highOrderPoint2(int order, int n, int nodenumber, int el0, int edge0, int el1, int edge1, int el2, int edge2 , int el3, int edge3, int el4, int edge4, int el5, int edge5);
    /*!
     * reserviert Speicherplatz fuer die Elemente
     */
    void prepareElementsforHighOrderPoints(int order);
    /*!
     * prueft, ob ein Wuerfel 1. Typs oder 2. Typs vorliegt
     */
    bool detect_cube_type(int i, int j, int l);
    /*!
     * erstellt zusaetzlichen Node
     */
    void insertPoint(int n, int i, int j, int l);
    /*!
     * fuegt fuer ein Tetraeder in der elementlist (Nummerierungen 2-5) alle highOrderPoints hinzu
     */
    void addHighOrderPoints(int order, int n, int i, int j, int l, int tetra );
    /*!
     * guckt, ob der Punkt von einem Facemittelpunkt bereits geaddet wurde
     */
    bool face_alreadyvisitied(faceElement &face, int n, int i, int j, int l, int tetra);
    /*!
     * fuegt noch separat die highOrderPoints auf den Faces hinzu
     */
    void add_centerPoints(int order,int n, int i, int j, int l, int tetra);
    /*!
     * fuegt noch die restlichen highOrderPOints hinzu, z.B. von den Randelementen
     */
    void generate_special_highOrderPoints(int order, int n, int i, int j, int l);
    /*!
     * erstellt Mittelpunkt eines Faces
     */
    node* balancePoint_face(faceElement &face);
    /*!
     * erstellt alle highOrderPoints
     */
    void generate_allhighOrderPoints(int order, int n);
};

