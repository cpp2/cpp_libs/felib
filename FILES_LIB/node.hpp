/*!
 * \file node.hpp
 * \brief node-Klasse, abgeleitet von point; enthält zusätzlich Randinformationen
 */

#pragma once

#include <point.hpp>

class node : public point{
private:
    int boundary ; ///< Randinfo: 0 = inner; 1 = dirichlet; 2 = neumann

public:
    // Konstruktoren

    // initialisiere x und y mit gegebenen Werten.
    // Zuerst ist jeder Punkt KEIN Rand
    ///Konstruktor für 1D Punkt
    node(double x_init);
    ///Konstruktor für 2D Punkt
    node(double x_init, double y_init);
    ///Konstruktor für 3D Punkt
    node(double x_init, double y_init, double z_init);

    ///copy-Konstruktor
    node(const point &p);
    /// \brief Standardkonstruktor für leeren (inneren) Knoten
    node():point(), boundary(0){} ;
    /// \brief Standardkonstruktor für leeren (inneren) Knoten mit 0 Koordinaten der entsprechenden Dimension
    node(int dimension):point(dimension), boundary(0){} ;
    /// \brief Getter für Rand-Information
    int getBoundaryInfo() const;
    /// \brief Setters fuer Koordinaten und Randinfo (1D, 2D, 3D)
    void setNodeValues(double x_in, int boundary);
    void setNodeValues(double x_in, double y_in, int boundary);
    void setNodeValues(double x_in, double y_in, double z_in, int boundary);

    /// \brief Prueft ob node am Dirichlet-Rand liegt.
    bool isDirichlet ();

    /// \brief Setter fuer Dirichlet-Info
    ///Setzen des DirichletRandes
    void setDirichlet (bool dirichlet_in);

    /// \brief Überladen der logischen Vergleiche für Knoten
    bool operator==(node &p);
    /// \brief Überladen der logischen Vergleiche für Knoten
    bool operator!=(node &p);



    /*!
     * fügt auf der Kante zwischen 2 nodes einen neuen mit affinem Verhältnis affine hinzu
     */
    node* newAffinePoint(double affine, node &p2);
    /// \brief Bestimmt den Mittelpunkt dreier Nodes
    node* balancepoint_triangle( node p2 , node p3);

    /// \brief Print-Funktion
    void print();
    ///Überladen des << Operators für Knoten
    friend std::ostream & operator<< (std::ostream & out, const node & n);

};



