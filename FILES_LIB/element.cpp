
#include "./element.hpp"


element::element(int T1, int T2, int T3) {nodelist.push_back(T1); nodelist.push_back(T2); nodelist.push_back(T3) ; }
element::element(int T1, int T2, int T3, int T4) {nodelist.push_back(T1); nodelist.push_back(T2);
                                                  nodelist.push_back(T3); nodelist.push_back(T4);}

void element::allocateNodelist(int n)
{
    nodelist.resize(n);
}


void element::setBasicElementStuff(std::vector<int> nodelist)
{
    this->nodelist = nodelist;
}

void element::setTransStuff(std::vector<point*> points)
{
    transstuff.fill(points);
}

void element::setNodeLukas(int i, const int &n)
{
    nodelist[i] = n;
}

int element::getNodeIndex(int i) const
{
    return nodelist[i] ;
}


int element::getElementSize() const
{
    return nodelist.size() ;
}

transformationStuff * element::getTransformation()
{
    return &transstuff;
}


void element::insertPoint(int T){
    nodelist.push_back(T);
}


void element::setNode(int nodenumber, int T){
        nodelist[T]=nodenumber;
    }

std::vector<int> * element::getNodelist()
{
    return &nodelist;
}

std::ostream & operator<< (std::ostream & out, const element & elem)
{
    out << "(";
    LOOP(index, elem.getElementSize() -1 )
        out << elem.getNodeIndex(index) << ", ";
    out << elem.getNodeIndex(elem.getElementSize() -1 ) ;
    out << ")";

    return out;
}
