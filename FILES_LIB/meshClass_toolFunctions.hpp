
#include "./meshClass.hpp"







#define add_Faces_T_s_T_t_T_u(s, t, u, n) { \
    LOOP(l,n)\
    LOOP(j,n)\
    LOOP(i,n){\
    std::vector<int> faceOpp=detect_faceContainers(i,j,l, n, s,t,u);\
    std::vector<int> vertices=detect_VerticesofElement(i, j, l, n , s,t,u);\
    bool boundary;\
    if ( ((i+j+l) % 2)  ==0){\
        boundary=check_boundary(vertices[0], vertices[1], vertices[2]);\
        if (boundary){\
            add_Face(vertices[0], vertices[1], vertices[2], faceOpp[0] , -1);\
            elementlist[faceOpp[0]].insertFace(facelist.size()-1);\
        }\
        else{\
            add_Face(vertices[0], vertices[1], vertices[2], faceOpp[0] , faceOpp[1]  );\
            elementlist[faceOpp[0]].insertFace(facelist.size()-1);\
            elementlist[faceOpp[1]].insertFace(facelist.size()-1);\
        }\
    }\
}\
}

#define add_boundary_faces(i)     vertices={elementlist[faceOpp[i]].getNodeIndex(0),elementlist[faceOpp[i]].getNodeIndex(1),elementlist[faceOpp[i]].getNodeIndex(2),elementlist[faceOpp[i]].getNodeIndex(3)};\
    boundary=check_boundary(vertices[0], vertices[1], vertices[2]);\
    if(boundary){\
        add_Face(vertices[0], vertices[1], vertices[2], faceOpp[0+i] , -1);\
        elementlist[faceOpp[i]].insertFace(facelist.size()-1);\
    }\
    boundary=check_boundary(vertices[0], vertices[1], vertices[3]);\
    if(boundary){\
        add_Face(vertices[0], vertices[1], vertices[3], faceOpp[0+i] , -1);\
        elementlist[faceOpp[i]].insertFace(facelist.size()-1);\
    }\
    boundary=check_boundary(vertices[1], vertices[2], vertices[3]);\
    if(boundary){\
        add_Face(vertices[1], vertices[2], vertices[3], faceOpp[0+i] , -1);\
        elementlist[faceOpp[i]].insertFace(facelist.size()-1);\
    }\
    boundary=check_boundary(vertices[0], vertices[2], vertices[3]);\
    if(boundary){\
        add_Face(vertices[0], vertices[2], vertices[3], faceOpp[0+i] , -1);\
        elementlist[faceOpp[i]].insertFace(facelist.size()-1);\
    }
