/*!
 * \file transformationStuff.hpp
 *  \brief Transformationswerkzeugkasten
 */

#pragma once

#include <point.hpp>
#include <denseMatrix2.hpp>



/**
 * Transformationswerkzeugkasten
 * Enthaelt einen Zeiger auf die Transformationsmatrix, sowie die Translation
 * der Transformation und die vorberechnete Matrix B^-T*B^-1.
 */
class transformationStuff{

private:
    denseMatrix2 B_T ; ///< Linearer Anteil der affin linearen Transformation
    denseMatrix2 BTB ; ///< B^-T*B^-1
    point b_affin    ; ////< Translationsanteil der Transformation

public:
    ///Leerer Konstruktor
    transformationStuff();
    transformationStuff(int dim) ;
    ~transformationStuff();

    ///Transformiere einen Punkt vom Referenzdreieck in das tatsächliche Rechengebiet
    void transform(const point &xRef, point &xGlobal);
    ///Rücktransformation vom physikalischen ins Referenzelement
    void transform_back(point &xGlobal, point &xRef);

    /**Berechne die Transformationsmatrix und setze einen Zeiger auf den Punkt die Translation der Transformation
     *
     * @param[in] vector<point*> points, die Ecken des Elements, auf das transformiert werden soll
     */
    void fill(std::vector<point*> points);
    ///Berechne B^-T*B^-1.
    void compBTB();
    ///Getter für Linearteil
    denseMatrix2 * getB_T();
    ///Getter für B^-T*B^-1
    denseMatrix2 * getBTB();
    ///Getter für Translationsteil
    point * getb_affin();

} ;




