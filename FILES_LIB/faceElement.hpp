
/*!\file face.hpp
 * \brief face Klasse, abgeleitet von element, enthält zusätzlich die Information, welche Elemente benachbart sind
 */
#pragma once

#include "./element.hpp"

/*!\brief Klasse für Oberflächenelemente, abgeleitet von element
 *
 */
class faceElement : public element{
protected:
    int neighbour1;///< Index des ersten angrenzenden Elementes
    int neighbour2;///< Index des zweiten angrenzenden Elementes

public:
    ///Konstruktor für ein leeres Oberflächenelement
    faceElement():element(),neighbour1(-10),neighbour2(-10){};
    faceElement(int i, int j, int k):element(i,j,k),neighbour1(-10),neighbour2(-10){}
    ///Getter
    int getNeighbour1();
    //Getter
    int getNeighbour2();
    ///Eingabe der Nachbarn und korrespondierender Knoten
    int fillFace(int n1, int n2, std::vector<int> nodelist);
    ///Setzen des ersten Nachbarn
    void setNeighbour1(int T);
    ///Setzen des zweiten Nachbarn
    void setNeighbour2(int T);
    ///Funktion, um Informationen über Indizes der benachbarten Elemente zu holen
    int getNeighbour(int neighbour) const ;
    ///Liefert für ein angrenzendes Element den Index des anderen Nachbarn
    int getNeighbourFromElement(int element);
    ///Abfrage, ob es sich hier um eine Randoberfläche handelt
    int isBoundary();
    ///Vergleiche von faceElementen
    bool operator==(const faceElement &fIn);
    ///Überladen des << Operators für Oberflächenelemente
    friend std::ostream & operator<< (std::ostream & out, const faceElement & face);
};



