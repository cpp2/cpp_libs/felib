
#include "./volumeElement.hpp"

volumeElement::volumeElement():element(){}

volumeElement::volumeElement(int T1, int T2, int T3, int T4):element(T1, T2, T3, T4){}


void volumeElement::insertFace(int T){
    faces.push_back(T);
}

void volumeElement::print(){
    std::cout <<"Die Faces sind   " ;
    for (int i=0; i<faces.size(); i++)
        std::cout << faces[i]  <<" " ;

}

void volumeElement::setFace(int index, int f)
{
    if (index >= faces.size())
        faces.resize(index+1);

    faces[index] = f;
}

int volumeElement::getFaceIndex(int index) const
{
return faces[index] ;
}

int volumeElement::getNfaces() const
{
return faces.size() ;
}

std::ostream & operator<< (std::ostream & out, const volumeElement & elem) {

    out << "(";
    LOOP(index, elem.getNfaces() -1)
        out << elem.getFaceIndex(index) << ", ";
    out << elem.getFaceIndex(elem.getNfaces() -1) ;
    out << ")";

    return out;
}
