#include "./node.hpp"

node::node(double x_init): point(x_init), boundary(0) {};

node::node(double x_init, double y_init): point(x_init,y_init), boundary(0) {};

node::node(double x_init, double y_init, double z_init): point(x_init,y_init,z_init), boundary(0) {};

node::node(const point &p):point(p), boundary(false)
{
}

int node::getBoundaryInfo() const {
    return boundary;
}

void node::setNodeValues(double x_in, int boundary_in){
    setX(x_in);
    this->boundary=boundary_in;
}

void node::setNodeValues(double x_in, double y_in, int boundary_in){
    setX(x_in);
    setY(y_in);
    this->boundary=boundary_in;
}

void node::setNodeValues(double x_in, double y_in, double z_in, int boundary_in){
    setX(x_in);
    setY(y_in);
    setZ(z_in);
    this->boundary=boundary_in;
}

bool node::isDirichlet(){
    if (boundary == 1)
        return true ;
    else
        return false;
}

void node::setDirichlet (bool dirichlet_in) {
    if (dirichlet_in == true)
        this->boundary = 1 ;
    else
        this->boundary = 0;
}

bool node::operator== (node &p)
    {
    if ((point)(*this)==p)
        return true;
    else
        return false;
    }

bool node::operator!= (node &p)
    {
return !(this->operator ==(p));
    }

/*!
 * fuegt auf der Kante zwischen 2 nodes einen neuen mit affinem Verhaeltnis affine hinzu
 */
node* node::newAffinePoint(double affine, node &p2){
    node *P= new node();
    double x,y,z;
    x=this->getX()*(1.0 -affine) + p2.getX()*affine;
    y=this->getY()*(1.0 -affine) + p2.getY()*affine;
    z=this->getZ()*(1.0 -affine) + p2.getZ()*affine;
    P->setNodeValues(x,y,z,0);
    return P;
}


node* node::balancepoint_triangle( node p2 , node p3){
    return new node( (this->getX() + p2.getX()  + p3.getX()) * (double)1/(double)3  , \
                    (this->getY() + p2.getY()  + p3.getY() )* (double)1/(double)3 );
}

void node::print(){
    point::print();
    std::cout << "\tRand: " << boundary ;
}

// Overload stream insertion operator out << c (friend)
std::ostream & operator<< (std::ostream & out, const node & n) {
    out << (point)n;
    out << "\tRand: " << n.getBoundaryInfo();
    return out;
}
