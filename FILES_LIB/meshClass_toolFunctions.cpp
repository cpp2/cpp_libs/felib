
#include "./meshClass_toolFunctions.hpp"




int meshClass::getnDimension() const
{
    return dimension;
}

int meshClass::getnBoundary() const
{
    return nBoundaryPoints;
}

int meshClass::getnElements() const
{
    return nElements;
}

int meshClass::getnFaces() const
{
    return nFaces;
}

int meshClass::getnNeumann() const
{
    return nNeumann;
}

int meshClass::getnOrder() const
{
    return nOrder;
}

int meshClass::getnPoints() const
{
    return nPoints;
}

volumeElement * meshClass::getElement(int Index)
{
    return &elementlist[Index] ;
}

std::vector<node> * meshClass::getNodelist(){
    return &nodelist ;
}

int meshClass::getnDirichlet() const
{
    return nDirichlet;
}

int meshClass::getnBoundaryFaces() const
{
    return nBoundaryFaces;
}

node* meshClass::returnNodefromNodelist(int T) {
    return &nodelist[T];
}

int meshClass::getGlobalNodeIndexFromElement(int elementIndex, int localPointIndex)
{
    return elementlist[elementIndex].getNodeIndex(localPointIndex);
}

node * meshClass::getGlobalNodeFromElement(int elementIndex, int localPointIndex)
{
    return &nodelist[elementlist[elementIndex].getNodeIndex(localPointIndex)];
}












void meshClass::set_tetrahedralVertices(int n){
    //    set_nPoints(n);
    LOOP(i,n+1) \
        LOOP(j,n+1) \
        LOOP(k,n+1){
        double x=(double) 1- (double) k/ (double) n;
        double y=(double) j/ (double) n;
        double z=(double) i/ (double) n;
        node p( x, y, z);
        if ( (x==0 || x==1) || (y==0 || y==1) || (z==0 || z==1) ){
            p.setDirichlet(true);
        }
        insertNode(p);
    }

}


std::vector<int> meshClass::get_cubeVertices(int i, int j, int l, int n){
    std::vector<int> nodenumbers;
    //rechtsunten: Index des rechten, vorderen unteren Punktes im Würfel
    int rechtsunten= i+j*(n+1) + l* (n+1)*(n+1);
    nodenumbers.push_back(rechtsunten);
    //unten, vorne rechts
    nodenumbers.push_back(rechtsunten+1);
    //unten, rechts hinten
    nodenumbers.push_back(rechtsunten+n+1);
    //unten, links hinten
    nodenumbers.push_back(rechtsunten+n+2);
    //obere Flaeche analog
    nodenumbers.push_back(rechtsunten+(n+1)*(n+1));
    nodenumbers.push_back(rechtsunten+(n+1)*(n+1)+1);
    nodenumbers.push_back(rechtsunten+(n+1)*(n+1)+n+1);
    nodenumbers.push_back(rechtsunten+(n+1)*(n+1)+n+2);
    return nodenumbers;
}
void meshClass::push_Tetrahedron(int T1, int T2, int T3, int T4){
    volumeElement tetrahedron(T1, T2, T3, T4);
    elementlist.push_back(tetrahedron);
}

void meshClass::add_Tetrahedrons(int i, int j, int l,  int mod, int n){
    std::vector<int> nodenumbers= get_cubeVertices(i,j,l,n);
    int T1=nodenumbers[0], T2=nodenumbers[1], T3=nodenumbers[2], T4=nodenumbers[3];
    int T5=nodenumbers[4], T6=nodenumbers[5], T7=nodenumbers[6], T8=nodenumbers[7];

    //Orientierung der Wuerfel
    //Typ 1
    if ((i+j+l+mod) % 2 ==0){
        push_Tetrahedron(T2, T3, T5, T8); //eingezeichnet: http://materials.lehrerweb.at/fileadmin/lehrerweb/materials/as/sek/webs/m/geom_koerper/konstruktion_tetraeder_wuerfel.htm
        push_Tetrahedron(T1, T2, T3, T5);
        push_Tetrahedron(T2, T3, T4, T8);
        push_Tetrahedron(T2, T5, T6, T8);
        push_Tetrahedron(T3, T5, T7, T8);
    }
    //Typ2
    if ((i+j+l+mod) % 2 ==1){
        push_Tetrahedron(T1, T4, T6, T7);  // http://www.zahlreich.de/messages/68409/11813.html
        push_Tetrahedron(T1, T2, T4, T6);
        push_Tetrahedron(T1, T3, T4, T7);
        push_Tetrahedron(T4, T6, T7, T8);
        push_Tetrahedron(T1, T5, T6, T7);
    }
    increase_nElements(5);
}

void meshClass::build_allTetrahedrons(int n){
    int mod =0;
    LOOP(l,n){
        LOOP(j,n){
            LOOP(i,n){
                add_Tetrahedrons(i,j,l, mod, n);
            }
            //if (j==n-1) mod=mod+1;

        }}
}

bool meshClass::check_boundary(int T_i, int T_j, int T_l){
    double x_i, y_i, x_j, z_i, y_j, z_j, x_l, y_l, z_l;
    x_i=nodelist[T_i].getX(); y_i=nodelist[T_i].getY(); z_i=nodelist[T_i].getZ();
    x_j=nodelist[T_j].getX(); y_j=nodelist[T_j].getY(); z_j=nodelist[T_j].getZ();
    x_l=nodelist[T_l].getX(); y_l=nodelist[T_l].getY(); z_l=nodelist[T_l].getZ();

    if (x_i==0 && x_j==0 && x_l ==0)
        return true;
    if (x_i==1 && x_j==1 && x_l ==1)
        return true;
    if (y_i==0 && y_j==0 && y_l ==0)
        return true;
    if (y_i==1 && y_j==1 && y_l ==1)
        return true;
    if (z_i==0 && z_j==0 && z_l ==0)
        return true;
    if (z_i==1 && z_j==1 && z_l ==1)
        return true;

    return false;
}

void meshClass::add_Face(int i, int j,  int l, int neighbour1, int neighbour2){
    faceElement flaeche(i,j,l);
    flaeche.setNeighbour1(neighbour1);
    flaeche.setNeighbour2(neighbour2);
    nFaces=nFaces+1;
    facelist.push_back(flaeche);
    if (neighbour2== -1)
        nBoundaryFaces++;
}

std::vector<int> meshClass::detect_faceContainers(int i, int j, int l, int n, int T_i, int T_j, int T_l){
    std::vector<int> faceOpp;
    int shift= i*5 + j*n*5 + 5*l*(n*n);
    if (T_i==1 && T_j==3 && T_l==5)
        faceOpp={shift+1 , shift-4};
    if (T_i==3 && T_j==5 && T_l==7)
        faceOpp={shift+4 , shift-2 };
    if (T_i==1 && T_j==2 && T_l==3)
        faceOpp={shift+1 , shift-5*(n*n)+4};
    if (T_i==2 && T_j==3 && T_l==4)
        faceOpp={shift+2 , shift-5*(n*n)+3 };
    if (T_i==2 && T_j==6 && T_l==8)
        faceOpp={shift+3 , shift+3+6 };
    if (T_i==2 && T_j==4 && T_l==8)
        faceOpp={shift+2 , shift+2+5 };
    if (T_i==5 && T_j==6 && T_l==8)
        faceOpp={shift+3 , shift+1+5*(n*n) };
    if (T_i==5 && T_j==7 && T_l==8)
        faceOpp={shift+4 , shift+2+5*(n*n) };
    if (T_i==1 && T_j==2 && T_l==5)
        faceOpp={shift+1 , shift-(1*5*n)+2 };
    if (T_i==2 && T_j==5 && T_l==6)
        faceOpp={shift+3 , shift-(1*n*5)+3 };
    if (T_i==3 && T_j==4 && T_l==8)
        faceOpp={shift+2 , shift+(5*n)+1 };
    if (T_i==3 && T_j==7 && T_l==8)
        faceOpp={shift+4 , shift+(5*n)+4 };
    if (T_i==2 && T_j==3 && T_l==5)
        faceOpp={shift+0 , shift+1 };
    if (T_i==2 && T_j==5 && T_l==8)
        faceOpp={shift+0 , shift+3 };
    if (T_i==2 && T_j==3 && T_l==8)
        faceOpp={shift+0 , shift+2 };
    if (T_i==3 && T_j==5 && T_l==8)
        faceOpp={shift+0 , shift+4 };

    return faceOpp;
}

std::vector<int> meshClass::detect_VerticesofElement(int i, int j, int l, int n, int T_i, int T_j, int T_l){
    std::vector<int> faceOpp;
    std::vector<int> vertices;
    if (T_i==1 && T_j==3 && T_l==5){
        faceOpp=detect_faceContainers(i,j,l, n, 1,3,5);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(2),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==3 && T_j==5 && T_l==7){
        faceOpp=detect_faceContainers(i,j,l, n, 3,5,7);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(2)};
    }
    if (T_i==2 && T_j==3 && T_l==4){
        faceOpp=detect_faceContainers(i,j,l, n, 2,3,4);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(2)};
    }
    if (T_i==1 && T_j==2 && T_l==3){
        faceOpp=detect_faceContainers(i,j,l, n, 1,2,3);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(2)};
    }
    if (T_i==2 && T_j==6 && T_l==8){
        faceOpp=detect_faceContainers(i,j,l, n, 2,6,8);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(2),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==2 && T_j==4 && T_l==8){
        faceOpp=detect_faceContainers(i,j,l, n, 2,4,8);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(2),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==5 && T_j==6 && T_l==8){
        faceOpp=detect_faceContainers(i,j,l, n, 5,6,8);
        vertices={elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(2),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==5 && T_j==7 && T_l==8){
        faceOpp=detect_faceContainers(i,j,l, n, 5,7,8);
        vertices={elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(2),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==1 && T_j==2 && T_l==5){
        faceOpp=detect_faceContainers(i,j,l, n, 1,2,5);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==2 && T_j==5 && T_l==6){
        faceOpp=detect_faceContainers(i,j,l, n, 2,5,6);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(2)};
    }
    if (T_i==3 && T_j==4 && T_l==8){
        faceOpp=detect_faceContainers(i,j,l, n, 3,4,8);
        vertices={elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(2),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==3 && T_j==7 && T_l==8){
        faceOpp=detect_faceContainers(i,j,l, n, 3,7,8);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(2),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==2 && T_j==3 && T_l==5){
        faceOpp=detect_faceContainers(i,j,l, n, 2,3,5);
        vertices={elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(2),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==2 && T_j==5 && T_l==8){
        faceOpp=detect_faceContainers(i,j,l, n, 2,5,8);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==2 && T_j==3 && T_l==8){
        faceOpp=detect_faceContainers(i,j,l, n, 2,3,8);
        vertices={elementlist[faceOpp[0]].getNodeIndex(0),elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(3)};
    }
    if (T_i==3 && T_j==5 && T_l==8){
        faceOpp=detect_faceContainers(i,j,l, 2, 3,5,8);
        vertices={elementlist[faceOpp[0]].getNodeIndex(1),elementlist[faceOpp[0]].getNodeIndex(2),elementlist[faceOpp[0]].getNodeIndex(3)};
    }


    return vertices;
}

void meshClass::add_special_Faces(int n){
    std::vector<int> vertices,faceOpp;
    bool boundary;
    LOOP(l,n)
    LOOP(j,n)
    LOOP(i,n){
        if ( (i+j+l) % 2 ==1 ){
            int index=i*5+j*n*5+5*l*(n*n);
            faceOpp={ index+0, index+1, index+2, index+3, index+4};

            //fuer die Randfaces
            add_boundary_faces(1);
            add_boundary_faces(2);
            add_boundary_faces(3);
            add_boundary_faces(4);

            //noch die Faces fuer die inneren Tetraeder
            if( (i+j+l)% 2 ==1){
                faceOpp={ index+0, index+1, index+2, index+3, index+4};
                vertices={  elementlist[faceOpp[0]].getNodeIndex(0) ,  elementlist[faceOpp[0]].getNodeIndex(1) , elementlist[faceOpp[0]].getNodeIndex(2) , elementlist[faceOpp[0]].getNodeIndex(3)};

                //                //fuer Face T1,T4,T6
                add_Face(vertices[0], vertices[1], vertices[2], faceOpp[0]  , faceOpp[1]);
                elementlist[index].insertFace(facelist.size()-1);
                elementlist[index+1].insertFace(facelist.size()-1);
                //                //fuer Face T1,T4,T7
                add_Face(vertices[0], vertices[1], vertices[3], faceOpp[0]  , faceOpp[2]);
                elementlist[index].insertFace(facelist.size()-1);
                elementlist[index+2].insertFace(facelist.size()-1);
                //                //fuer Face T4,T6,T7
                add_Face(vertices[1], vertices[2], vertices[3], faceOpp[0]  , faceOpp[3]);
                elementlist[index].insertFace(facelist.size()-1);
                elementlist[index+3].insertFace(facelist.size()-1);
                //                //fuer Face T1,T6,T7
                add_Face(vertices[0], vertices[2], vertices[3], faceOpp[0]  , faceOpp[4]);
                elementlist[index].insertFace(facelist.size()-1);
                elementlist[index+4].insertFace(facelist.size()-1);

            }
        }

    }
}

void meshClass::build_all_Faces(int n){
    add_Faces_T_s_T_t_T_u(1, 3, 5, n);     //fuegt alle Faces der Wuerfel Typ1 hinzu, welche zugleich Faces derer vom Typ2 sind
    add_Faces_T_s_T_t_T_u(3, 5, 7, n);
    add_Faces_T_s_T_t_T_u(1, 2, 3, n);
    add_Faces_T_s_T_t_T_u(2, 3, 4, n);
    add_Faces_T_s_T_t_T_u(2, 6, 8, n);
    add_Faces_T_s_T_t_T_u(2, 4, 8, n);
    add_Faces_T_s_T_t_T_u(5, 6, 8, n);
    add_Faces_T_s_T_t_T_u(5, 7, 8, n);
    add_Faces_T_s_T_t_T_u(1, 2, 5, n);
    add_Faces_T_s_T_t_T_u(2, 5, 6, n);
    add_Faces_T_s_T_t_T_u(3, 4, 8, n);
    add_Faces_T_s_T_t_T_u(3, 7, 8, n);
    add_Faces_T_s_T_t_T_u(2, 3, 5, n);
    add_Faces_T_s_T_t_T_u(2, 5, 8, n);
    add_Faces_T_s_T_t_T_u(2, 3, 8, n);
    add_Faces_T_s_T_t_T_u(3, 5, 8, n);
    add_special_Faces(n);
}


std::vector<node*> meshClass::compute_affineNodes(int anzahl, volumeElement &tetrahedron ,int eckpunkt1, int eckpunkt2){
    std::vector<node*> affineNodes;
    double affine;
    for (int i=0; i<anzahl; i++){
        affine= (double) (i+1)/  (double) (anzahl+1);
        affineNodes.push_back( nodelist[tetrahedron.getNodeIndex(eckpunkt1)].newAffinePoint ( affine,  nodelist[tetrahedron.getNodeIndex(eckpunkt2)]  )  );
    }
    double x,y,z;
    LOOP(i,anzahl){
        x=affineNodes[i]->getX(); y=affineNodes[i]->getY(); z=affineNodes[i]->getZ();
        if ( (x==0 || x==1) || (y==0 || y==1) || (z==0 || z==1) ){
            affineNodes[i]->setDirichlet(true);
        }
        insertNode(*(affineNodes[i]));
    }
    return affineNodes;
}

node* meshClass::compute_middleNode( volumeElement &tetrahedron ,int eckpunkt1, int eckpunkt2){
    node *P;
    double affine;
    affine= (double) 1/  (double) (2);
    P=( nodelist[tetrahedron.getNodeIndex(eckpunkt1)].newAffinePoint ( affine,  nodelist[tetrahedron.getNodeIndex(eckpunkt2)]  )  );
    double x,y,z;
    x=P->getX(); y=P->getY(); z=P->getZ();
    if ( (x==0 || x==1) || (y==0 || y==1) || (z==0 || z==1) ){
        P->setDirichlet(true);
    }
    insertNode(*P);
    return P;
}

void meshClass::insert_highOrderPoint(int order, int n, int nodenumber, int el0, int edge0, int el1, int edge1, int el2, int edge2 , int el3, int edge3, int el4, int edge4){
    if (order==2){
        if (el0>-1 && el0<n*n*n*5) {elementlist[el0].setNode(nodenumber, edge0+4);}
        if (el1>-1 && el1<n*n*n*5) {elementlist[el1].setNode(nodenumber, edge1+4);}
        if (el2>-1 && el2<n*n*n*5) {elementlist[el2].setNode(nodenumber, edge2+4);}
        if (el3>-1 && el3<n*n*n*5) {elementlist[el3].setNode(nodenumber, edge3+4);}
        if (el4>-1 && el4<n*n*n*5) {elementlist[el4].setNode(nodenumber, edge4+4);}
    }
    if (order==3){
        if (el0>-1 && el0<n*n*n*5) {elementlist[el0].setNode(nodenumber-1, 2*edge0+4);elementlist[el0].setNode(nodenumber, 2*edge0+1+4);}
        if (el1>-1 && el1<n*n*n*5) {elementlist[el1].setNode(nodenumber-1, 2*edge1+4);elementlist[el1].setNode(nodenumber, 2*edge1+1+4);}
        if (el2>-1 && el2<n*n*n*5) {elementlist[el2].setNode(nodenumber-1, 2*edge2+4);elementlist[el2].setNode(nodenumber, 2*edge2+1+4);}
        if (el3>-1 && el3<n*n*n*5) {elementlist[el3].setNode(nodenumber-1, 2*edge3+4);elementlist[el3].setNode(nodenumber, 2*edge3+1+4);}
        if (el4>-1 && el4<n*n*n*5) {elementlist[el4].setNode(nodenumber-1, 2*edge4+4);elementlist[el4].setNode(nodenumber, 2*edge4+1+4);}
    }
}

void meshClass::insert_highOrderPoint2(int order, int n, int nodenumber, int el0, int edge0, int el1, int edge1, int el2, int edge2 , int el3, int edge3, int el4, int edge4, int el5,int edge5){
    if (order==2){
        if (el0>-1 && el0<n*n*n*5) {elementlist[el0].setNode(nodenumber, edge0+4);}
        if (el1>-1 && el1<n*n*n*5) {elementlist[el1].setNode(nodenumber, edge1+4);}
        if (el2>-1 && el2<n*n*n*5) {elementlist[el2].setNode(nodenumber, edge2+4);}
        if (el3>-1 && el3<n*n*n*5) {elementlist[el3].setNode(nodenumber, edge3+4);}
        if (el4>-1 && el4<n*n*n*5) {elementlist[el4].setNode(nodenumber, edge4+4);}
        if (el5>-1 && el4<n*n*n*5) {elementlist[el5].setNode(nodenumber, edge5+4);}
    }
    if (order==3){
        if (el0>-1 && el0<n*n*n*5) {elementlist[el0].setNode(nodenumber-1, 2*edge0+4);elementlist[el0].setNode(nodenumber, 2*edge0+1+4);}
        if (el1>-1 && el1<n*n*n*5) {elementlist[el1].setNode(nodenumber-1, 2*edge1+4);elementlist[el1].setNode(nodenumber, 2*edge1+1+4);}
        if (el2>-1 && el2<n*n*n*5) {elementlist[el2].setNode(nodenumber-1, 2*edge2+4);elementlist[el2].setNode(nodenumber, 2*edge2+1+4);}
        if (el3>-1 && el3<n*n*n*5) {elementlist[el3].setNode(nodenumber-1, 2*edge3+4);elementlist[el3].setNode(nodenumber, 2*edge3+1+4);}
        if (el4>-1 && el4<n*n*n*5) {elementlist[el4].setNode(nodenumber-1, 2*edge4+4);elementlist[el4].setNode(nodenumber, 2*edge4+1+4);}
        if (el5>-1 && el5<n*n*n*5) {elementlist[el5].setNode(nodenumber-1, 2*edge5+4);elementlist[el5].setNode(nodenumber, 2*edge4+1+4);}
    }
}

int meshClass::gleich(int n, int i, int j, int l){
    return 0;
}

int meshClass::rechts(int n, int i, int j, int l){
    if (i==0)
        return -5*n*n*n*n;
    else
        return -5;
}

int meshClass::unten(int n, int i, int j, int l) {
    if (l==0)
        return (-5)*n*n*n*n;
    else
        return -5*n*n;
}

int meshClass::links(int n, int i, int j, int l) {
    if (i==n-1)
        return -5*n*n*n*n;
    else
        return +5;
}

int meshClass::oben(int n, int i, int j, int l) {
    if (l==n-1)
        return -5*n*n*n*n;
    else
        return 5*n*n;
}

int meshClass::vorne(int n, int i, int j, int l) {
    if (j==0)
        return -5*n*n*n*n;
    else
        return -5*n;
}

int meshClass::hinten(int n, int i, int j, int l) {
    if(j==n-1)
        return -5*n*n*n*n;
    else
        return 5*n;
}

int meshClass::T1_T4_T6_T7(){
    return +0;
}

int meshClass::T1_T2_T4_T6(){
    return +1;
}

int meshClass::T1_T3_T4_T7(){
    return +2;
}

int meshClass::T4_T6_T7_T8(){
    return +3;
}

int meshClass::T1_T5_T6_T7(){
    return +4;
}

int meshClass::T2_T3_T5_T8(){
    return +0;
}

int meshClass::T1_T2_T3_T5(){
    return +1;
}

int meshClass::T2_T3_T4_T8(){
    return +2;
}

int meshClass::T2_T5_T6_T8(){
    return +3;
}

int meshClass::T3_T5_T7_T8(){
    return +4;
}


int meshClass::T1_T5_T6_T7(int P1, int P2){
    if (P1==1 && P2==5)
        return 0;
    if (P1==5 && P2==6)
        return 1;
    if (P1==1 && P2==6)
        return 2;
    if (P1==1 && P2==7)
        return 3;
    if (P1==5 && P2==7)
        return 4;
    if (P1==6 && P2==7)
        return 5;
    return -1;
}

int meshClass::T4_T6_T7_T8(int P1, int P2){
    if (P1==4 && P2==6)
        return 0;
    if (P1==6 && P2==7)
        return 1;
    if (P1==4 && P2==7)
        return 2;
    if (P1==4 && P2==8)
        return 3;
    if (P1==6 && P2==8)
        return 4;
    if (P1==7 && P2==8)
        return 5;
    return -1;
}
int T1_T2_T4_T6(int P1, int P2){
    if (P1==1 && P2==2)
        return 0;
    if (P1==2 && P2==4)
        return 1;
    if (P1==1 && P2==4)
        return 2;
    if (P1==1 && P2==6)
        return 3;
    if (P1==2 && P2==6)
        return 4;
    if (P1==4 && P2==6)
        return 5;
    return -1;
}

void meshClass::prepareElementsforHighOrderPoints(int order){
    if (order==2){
        LOOP(i, nElements)
                        LOOP(j,6)
                        elementlist[i].insertPoint(-100);
    }
    if (order==3){
        LOOP(i, nElements)
                        LOOP(j,16)
                        elementlist[i].insertPoint(-100);
    }
}

void meshClass::addHighOrderPoints(int order, int n, int i, int j, int l, int tetra ){
    int index=i*5 + j*n*5 + 5*l*(n*n);
    {
        if ( (i+j+l)%2==0){
            if (tetra==1){
                compute_affineNodes(order-1, elementlist[index+T1_T2_T3_T5()], 0, 1);   insert_highOrderPoint(order, n,nodelist.size()-1, index+gleich(n,j,i,l)+T1_T2_T3_T5(),0, index+vorne(n,i,j,l)+T1_T3_T4_T7(),1,  index+vorne(n,i,j,l)+unten(n,i,j,l)+T3_T5_T7_T8(),5, index+unten(n,i,j,l)+T1_T5_T6_T7(),1, -1,-1 );
                compute_affineNodes(order-1, elementlist[index+T1_T2_T3_T5()], 1, 2);   insert_highOrderPoint2(order, n,nodelist.size()-1, index+gleich(n,j,i,l)+T1_T2_T3_T5(),1, index+gleich(n,i,j,l)+T2_T3_T5_T8(),0, index+gleich(n,i,j,l)+T2_T3_T4_T8(),0,  (index+unten(n,i,j,l)+T1_T5_T6_T7()),5, index+unten(n,i,j,l)+T4_T6_T7_T8(),1, index+unten(n,i,j,l)+T1_T4_T6_T7(),5 );
                compute_affineNodes(order-1, elementlist[index+T1_T2_T3_T5()], 2, 0);   insert_highOrderPoint(order, n,nodelist.size()-1, index+gleich(n,j,i,l)+T1_T2_T3_T5(),2, index+rechts(n,i,j,l)+T1_T2_T4_T6(),1, index+unten(n,i,j,l)+rechts(n,i,j,l)+T2_T5_T6_T8(),5, index+unten(n,i,j,l)+T1_T5_T6_T7(),4,  -1,-1);
                compute_affineNodes(order-1, elementlist[index+T1_T2_T3_T5()], 0, 3);   insert_highOrderPoint(order, n,nodelist.size()-1, index+gleich(n,j,i,l)+T1_T2_T3_T5(),3, index+vorne(n,i,j,l)+T1_T3_T4_T7(),4,  index+rechts(n,i,j,l)+T1_T2_T4_T6(),4, index+vorne(n,i,j,l)+rechts(n,i,j,l)+T2_T3_T4_T8(),5,  -1,-1);
                compute_affineNodes(order-1, elementlist[index+T1_T2_T3_T5()], 1, 3);   insert_highOrderPoint2(order, n,nodelist.size()-1, index+gleich(n,j,i,l)+T1_T2_T3_T5(),4, index+gleich(n,i,j,l)+T2_T3_T5_T8(),2, index+gleich(n,i,j,l)+T2_T5_T6_T8(),0,  index+vorne(n,i,j,l)+T1_T3_T4_T7(),5,  index+vorne(n,i,j,l)+T4_T6_T7_T8(),2,index+vorne(n,i,j,l)+T1_T4_T6_T7(),4    );
                compute_affineNodes(order-1, elementlist[index+T1_T2_T3_T5()], 2, 3);   insert_highOrderPoint2(order, n,nodelist.size()-1, index+gleich(n,j,i,l)+T1_T2_T3_T5(),5, index+rechts(n,i,j,l)+T1_T2_T4_T6(),5, index+rechts(n,i,j,l)+T4_T6_T7_T8(),0, index+rechts(n,i,j,l)+T1_T4_T6_T7(),1, index+gleich(n,i,j,l)+T2_T3_T5_T8(),1,  index+gleich(n,i,j,l)+T3_T5_T7_T8(),0  );
            }
            if(tetra==2){
                compute_affineNodes(order-1, elementlist[index+T2_T3_T4_T8()] , 1, 2);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T2_T3_T4_T8(),1,   index+hinten(n,i,j,l)+T1_T2_T4_T6(),0, index+unten(n,i,j,l)+T4_T6_T7_T8(),5, index+unten(n,i,j,l)+hinten(n,i,j,l)+T2_T5_T6_T8(),1, -1,-1  );
                compute_affineNodes(order-1, elementlist[index+T2_T3_T4_T8()] , 2, 0);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T2_T3_T4_T8(),2,   index+links(n,i,j,l)+T1_T3_T4_T7(),0,  index+unten(n,i,j,l)+T4_T6_T7_T8(),4, index+unten(n,i,j,l)+links(n,i,j,l)+T3_T5_T7_T8(),1, -1,-1);
                compute_affineNodes(order-1, elementlist[index+T2_T3_T4_T8()] , 0, 3);  insert_highOrderPoint2(order, n, nodelist.size()-1, index+T2_T3_T4_T8(),3,   index+links(n,i,j,l)+T1_T5_T6_T7(),3,  index+links(n,i,j,l)+T1_T3_T4_T7(),3, index+gleich(n,i,j,l)+T2_T3_T5_T8(),3, index+gleich(n,i,j,l)+T2_T5_T6_T8(),3,index+links(n,i,j,l)+T1_T4_T6_T7(),3 );
                compute_affineNodes(order-1, elementlist[index+T2_T3_T4_T8()] , 1, 3);  insert_highOrderPoint2(order, n, nodelist.size()-1, index+T2_T3_T4_T8(),4,   index+hinten(n,i,j,l)+T1_T2_T4_T6(),3,  index+hinten(n,i,j,l)+T1_T5_T6_T7(),2, index+hinten(n,i,j,l)+T1_T4_T6_T7(),2, index+gleich(n,i,j,l)+T2_T3_T5_T8(),4, index+gleich(n,i,j,l)+T3_T5_T7_T8(),3   );
            }
            if(tetra==3){
                compute_affineNodes(order-1, elementlist[index+T2_T5_T6_T8()] , 2, 0);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T2_T5_T6_T8(),2, index+vorne(n,i,j,l)+T4_T6_T7_T8(),T4_T6_T7_T8(4,8),  index+vorne(n,i,j,l)+links(n,i,j,l)+T3_T5_T7_T8(),2, index+links(n,i,j,l)+T1_T5_T6_T7(),T1_T5_T6_T7(1,5), -1,-1);
            }
            if(tetra==4){
                compute_affineNodes(order-1, elementlist[index+T3_T5_T7_T8()] , 1, 3);  insert_highOrderPoint2(order, n, nodelist.size()-1, index+T3_T5_T7_T8(),4, index+oben(n,i,j,l)+T1_T2_T4_T6(),2,  index+oben(n,i,j,l)+T1_T3_T4_T7(),2, index+oben(n,i,j,l)+T1_T4_T6_T7(),0,  index+gleich(n,i,j,l)+T2_T3_T5_T8(),5, index+gleich(n,i,j,l)+T2_T5_T6_T8(),4);
            }

        }
    }
}

void meshClass::generate_special_highOrderPoints(int order, int n, int i, int j, int l){
    int index=i*5 + j*n*5 + 5*l*(n*n);

    if ( (i+j+l)%2 ==0){ //Quadrate Typ0
        if(i==0 || j==n-1){
            //Kante T3-T7
            compute_affineNodes(order-1, elementlist[index+T3_T5_T7_T8()] , 2, 0);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T3_T5_T7_T8(),2, index+rechts(n,i,j,l)+T4_T6_T7_T8(),T4_T6_T7_T8(4,8),   index+hinten(n,i,j,l)+T1_T5_T6_T7(),T1_T5_T6_T7(1,5),  -1,-1, -1,-1);
        }
        if (i==n-1 || j==n-1){
            //Kante T4-T8
            compute_affineNodes(order-1, elementlist[index+T2_T3_T4_T8()] , 2, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T2_T3_T4_T8(),5, index+links(n,i,j,l)+T1_T3_T4_T7(),4,   index+hinten(n,i,j,l)+T1_T2_T4_T6(),4 ,  -1,-1, -1,-1);
        }
        if (i==n-1 || l==n-1){
            //Kante T6-T8
            compute_affineNodes(order-1, elementlist[index+T2_T5_T6_T8()] , 2, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T2_T5_T6_T8(),5, index+links(n,i,j,l)+T1_T5_T6_T7(),4,   index+oben(n,i,j,l)+T1_T2_T4_T6(),1,  -1,-1, -1,-1);
        }
        if (j==0 || l==n-1){
            //Kante T5-T6
            compute_affineNodes(order-1, elementlist[index+T2_T5_T6_T8()] , 1, 2);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T2_T5_T6_T8(),1, index+vorne(n,i,j,l)+T4_T6_T7_T8(),5, index+oben(n,i,j,l)+T1_T2_T4_T6(),0,  -1,-1, -1,-1);
        }
        if(l==n-1 || i==0){
            //Kante T5-T7
            compute_affineNodes(order-1, elementlist[index+T3_T5_T7_T8()] , 1, 2);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T3_T5_T7_T8(),1, index+rechts(n,i,j,l)+T4_T6_T7_T8(),T4_T6_T7_T8(6,8),   index+oben(n,i,j,l)+T1_T3_T4_T7(),0,  -1,-1, -1,-1);
        }
        if (j==n-1 || l==n-1){
            //Kante T7-T8
            compute_affineNodes(order-1, elementlist[index+T3_T5_T7_T8()] , 2, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T3_T5_T7_T8(),5, index+hinten(n,i,j,l)+T1_T5_T6_T7(),1,   index+oben(n,i,j,l)+T1_T3_T4_T7(),1,  -1,-1, -1,-1);
        }
    }

    // ///Quadrate Typ1
    if ( (i+j+l)%2 ==1) {
        //Kanten T7-T8 hinten oben
        if(l==n-1 && j==n-1){
            compute_affineNodes(order-1, elementlist[index+T4_T6_T7_T8()], 2, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T4_T6_T7_T8(),5,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T1-T2 rechts unten
        if(l==0 && j==0){
            compute_affineNodes(order-1, elementlist[index+T1_T2_T4_T6()], 0, 1);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T2_T4_T6(),0,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kante T2-T4 links unten
        if (l==0 && i==n-1){
            compute_affineNodes(order-1, elementlist[index+T1_T2_T4_T6()], 1, 2);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T2_T4_T6(),1,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T3-T4 rechts unten
        if (l==0 && j==n-1){
            compute_affineNodes(order-1, elementlist[index+T1_T3_T4_T7()], 1, 2);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T3_T4_T7(),1,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T1-T3 rechts unten
        if(l==0 && i==0){
            compute_affineNodes(order-1, elementlist[index+T1_T3_T4_T7()], 0, 1);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T3_T4_T7(),0,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T5-T7 rechts unten
        if(l==n-1 && i==0){
            compute_affineNodes(order-1, elementlist[index+T1_T5_T6_T7()], 1, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T5_T6_T7(),4,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T1-T4 unten
        if(l==0 ){
            compute_affineNodes(order-1, elementlist[index+T1_T2_T4_T6()], 2, 0);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T2_T4_T6(),2,  index+T1_T3_T4_T7(),2,  index+T1_T4_T6_T7(),0,     -1,-1,-1,-1);
        }
        //Kanten T4-T7 unten
        if(j==n-1 ){
            compute_affineNodes(order-1, elementlist[index+T1_T4_T6_T7()], 1, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T4_T6_T7(),4,  index+T1_T3_T4_T7(),5,  index+T4_T6_T7_T8(),2,     -1,-1,-1,-1);
        }
        //Kante T1-T6 unten vorne
        if(j==0 ){
            compute_affineNodes(order-1, elementlist[index+T1_T2_T4_T6()], 0, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T2_T4_T6(),3,  index+gleich(n,i,j,l)+T1_T4_T6_T7(),2, index+gleich(n,i,j,l)+T1_T5_T6_T7(),2,    -1,-1,-1,-1);
        }
        //Kante T4-T6 links
        if(i==n-1){
            compute_affineNodes(order-1, elementlist[index+T1_T4_T6_T7()], 1, 2);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T4_T6_T7(),1,  index+gleich(n,i,j,l)+T1_T2_T4_T6(),5, index+gleich(n,i,j,l)+T4_T6_T7_T8(),0,    -1,-1,-1,-1);
        }
        //Kanten T1-T7 rechts
        if(i==0){
            compute_affineNodes(order-1, elementlist[index+T1_T3_T4_T7()], 0, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T3_T4_T7(),3,  index+T1_T4_T6_T7(),3, index+T1_T5_T6_T7(),3,   -1,-1,-1,-1);
        }
        //Kanten T1-T5 rechts-vorne
        if(j==0 && i==0){
            compute_affineNodes(order-1, elementlist[index+T1_T5_T6_T7()], 0, 1);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T5_T6_T7(),0,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T2-T6 links-vorne
        if(j==0 && i==n-1){
            compute_affineNodes(order-1, elementlist[index+T1_T2_T4_T6()], 1, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T2_T4_T6(),4,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T4-T8 links-hinten
        if(j==n-1 && i==n-1){
            compute_affineNodes(order-1, elementlist[index+T4_T6_T7_T8()], 0, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T4_T6_T7_T8(),3,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T3-T7 rechts-hinten
        if(j==n-1 && i==0){
            compute_affineNodes(order-1, elementlist[index+T1_T3_T4_T7()], 1, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T3_T4_T7(),4,  -1,-1,-1,-1,-1,-1,-1,-1);
        }

        //Kanten T5-T6 vorne oben
        if(l==n-1 && j==0){
            compute_affineNodes(order-1, elementlist[index+T1_T5_T6_T7()], 1, 2);    insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T5_T6_T7(),1,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T6-T8 vorne oben
        if(l==n-1 ){
            compute_affineNodes(order-1, elementlist[index+T4_T6_T7_T8()], 1, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T4_T6_T7_T8(),4,  -1,-1,-1,-1,-1,-1,-1,-1);
        }
        //Kanten T6-T7 oben
        if(l==n-1){
            compute_affineNodes(order-1, elementlist[index+T1_T5_T6_T7()], 2, 3);  insert_highOrderPoint(order, n, nodelist.size()-1, index+T1_T5_T6_T7(),5,  index+T4_T6_T7_T8(),1,   index+T1_T4_T6_T7(),5,   -1,-1,-1,-1);
        }
    }



}

node* meshClass::balancePoint_face(faceElement &face){
    node *P= new node();

    double x,y,z;
    x=( nodelist[face.getNodeIndex(0)].getX()+ nodelist[face.getNodeIndex(1)].getX() +nodelist[face.getNodeIndex(2)].getX() )*(1.0/3.0);
    y=( nodelist[face.getNodeIndex(0)].getY()+ nodelist[face.getNodeIndex(1)].getY() +nodelist[face.getNodeIndex(2)].getY() )*(1.0/3.0);
    z=( nodelist[face.getNodeIndex(0)].getZ()+ nodelist[face.getNodeIndex(1)].getZ() +nodelist[face.getNodeIndex(2)].getZ() )*(1.0/3.0);
    P->setNodeValues(x,y,z,0);
    if ( (x==0 || x==1) || (y==0 || y==1) || (z==0 || z==1) )
        P->setDirichlet(true);
    return P;
}

bool meshClass::face_alreadyvisitied(faceElement &face, int n, int i, int j, int l, int tetra){
    if (face.getNeighbour2() <0)
        return false;
    if (5*i+5*n*j+5*l*n*n+tetra > std::min( face.getNeighbour1(), face.getNeighbour2()) )
        return true;
    return false;
}

//void meshClass::add_centerPoints(int order,int n, int i, int j, int l, int tetra){
//    if (order==3){
//        int index=5*i+5*n*j+5*l*n*n+tetra;
//        volumeElement *tetrahedron=&elementlist[index];
//      for (int i=0; i<4; i++){
//          faceElement *face= &facelist[tetrahedron->getFaceIndex(i)];
//          node *P= balancePoint_face(*face);
//
//          if (!face_alreadyvisitied(face, n, i,j,l,tetra) )
//              insertNode(*P);
//
//              tetrahedron->setFace(16+i, nodelist.size()-1);
//              elementlist[tetrahedron->getNeighbour1()]->setFace(16+i, nodelist.size()-1);
//              elementlist[tetrahedron->getNeighbour2()]->setFace(16+i, nodelist.size()-1);
//
//
//
//
//
//      }
////
////      int neighbour=face.
//    }
//}





