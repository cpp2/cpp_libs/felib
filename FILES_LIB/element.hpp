/*!
 * \file element.hpp
 * \brief Elementklasse, enthält eine Liste von nodes; faceElement und volumeElement werden abgeleitet
 */


#pragma once

#include <vector>

#include <point.hpp>

#include "./transformationStuff.hpp"

/*!
 * \brief Klasse für die Gitterobjekte
 * */
class element{
public:
    std::vector<int> nodelist;///<Vektor mit den Nodes, die das Element enthält

    transformationStuff transstuff;///<Objekt für die linear-affine Transformation

public:
    ///Konstruktor mit drei Ecken, also Faces
    element(int T1, int T2, int T3);
    ///Konstruktor mit vier Ecken, als Tetraeder
    element(int T1, int T2, int T3, int T4);

    ///Standardkonstruktor für "leeres" Element
    element(){};

    /*!
     * legt Anzahl der Nodes in nodelist fest
     */
    void allocateNodelist(int n);
    /*!
     * speichert die linear affine Transformation ab
     */
    void setTransStuff(std::vector<point*> points);
    /*!
     * ein Setter fuer den i-ten Punkt des Elements
     */
    void setNodeLukas(int i, const int &n);

    /*!
     * Setzer fuer die gesamte nodelist
     */
    void setBasicElementStuff(std::vector<int> nodelist);
    /*!
     * Setter fuer i-ten Eintrag in der Nodelist
     */
    int getNodeIndex(int i) const ;
    /*!
     * gibt Anzahl der Eckpunkte des Elements wieder
     */
    int getElementSize() const ;
    /*!
     * gibt die affin lineare Transformation zurueck
     */
    transformationStuff * getTransformation();
    /*!
     * Getter fuer die nodelist
     */
    std::vector<int> * getNodelist();

    /*!
     * fuegt einen node in die nodelist ein, T:Stelle des nodes in der nodelist des Gitters
     */
    void insertPoint(int T);
    /*!
     * Setter fuer T-ten Eintrag in Nodelist
     */
    void setNode(int nodenumber, int T);

    ///Überladen des << Operators, so dass die Elementknoten ausgegeben werden
    friend std::ostream & operator<< (std::ostream & out, const element & elem);
} ;




