#include "./meshClass.hpp"


void meshClass::print()
{
    LOG(info) << "\nAusgabe des gesamten Gitters:\n" ;
    LOG(info) << "Dimension: \t" << dimension ;
    LOG(info) << "FEM-Ordnung: \t" << nOrder ;
    LOG(info) << "#Elemente: \t" << nElements << " (" << elementlist.size() << ")" ;
    LOG(info) << "#Points: \t" << nPoints << " (" << nodelist.size() << ")" ;
    LOG(info) << "#Faces: \t" << nFaces << " (" << facelist.size() << ")" ;
    int realNBoundaryPoints = 0;
    for (auto it : nodelist)
        if(it.isDirichlet())
            realNBoundaryPoints++;
    LOG(info) << "#Randpunkte: \t" << nBoundaryPoints  << " (" << realNBoundaryPoints << ")" ;
    LOG(info) << "#Dir.-punkte: \t" << nDirichlet ;
    LOG(info) << "#Neumann: \t" << nNeumann ;

    int realNBoundaryFaces = 0;
    for (auto it : facelist)
        if(it.isBoundary())
            realNBoundaryFaces++;
    LOG(info) << "#Randfaces: \t" << nBoundaryFaces  << " (" << realNBoundaryFaces << ")" << std::endl;

    if (1)
    {
        LOG(debug) << "Gib zuerst die Knotenliste mit " << nodelist.size() << " Knoten aus:" ;

        LOOP(nodeIndex, nodelist.size()){
            /* (point) castet das node zu point, damit die << Überladung der Basisklasse genutzt werden kann*/
            LOG(debug) << "Knoten " << std::setw(NUM_DIG(nodelist.size())) << nodeIndex << ": P = " << std::fixed << std::setprecision( 2 ) << std::setfill( '0' ) << nodelist[nodeIndex] ; }

        LOG(debug) << "\nGib die " << elementlist.size() << " Elemente aus." ;

        LOOP(elementIndex, elementlist.size() )
        {
            std::stringstream s;
            LOOP(locFace, dimension + 1)
            s << getNeighbour(elementIndex, locFace) << " ";

            /*(element) castet das volumeElement zu element, damit die << Überladung der Basisklasse genutzt werden kann*/
            LOG(debug) << "Element " << std::setw(NUM_DIG(elementlist.size())) << elementIndex << " P_index = " << (element) elementlist[elementIndex] << \
                "  face: " << elementlist[elementIndex] << " \tNachbarn: " << s.str();
        }

        LOG(debug) << "\nGib die " << facelist.size() << " Oberflaechenelemente aus:"  ;

        LOOP(faceIndex, facelist.size())
        {
            LOG(debug) << facelist[faceIndex] ;
        }
    }
}




void meshClass:: set_nPoints(int n){
    nPoints= (n+1)*(n+1)*(n+1);
}

void meshClass::increase_nPoints(int i){
    nPoints++;
}

void meshClass::increase_nElements(int j){
    nElements=nElements+j;
}

void meshClass::increase_nBoundary(int j)
{
    nBoundaryPoints = nBoundaryPoints + j;
    nDirichlet = nDirichlet + j;
}


void meshClass::setDimension(int d){

    dimension = d;
}

void meshClass::set_point(double x_in, double y_in, int nodeNumber){
    nodelist[nodeNumber].setX(x_in);
    nodelist[nodeNumber].setY(y_in);
}

void meshClass::set_boundary(bool dirichlet_in, int nodeNumber)
{
    if (dirichlet_in && !nodelist[nodeNumber].getBoundaryInfo())
    {
        nBoundaryPoints++;
        nDirichlet++;
    }
    else if (!dirichlet_in && nodelist[nodeNumber].getBoundaryInfo())
    {
        nBoundaryPoints--;
        nDirichlet--;
    }
    nodelist[nodeNumber].setDirichlet(dirichlet_in) ;
}

int meshClass::insertNode(const node n)
{
    nodelist.push_back(n);
    nPoints++;
    if (n.getBoundaryInfo()){
        nBoundaryPoints++;
        nDirichlet++;
    }
    return nodelist.size()-1;
}



void meshClass::defineRefElementFirstOrder(int dimensionIn)
{
    if (dimensionIn == 1)
    {
        dimension = dimensionIn;
        createTestMesh(2);
    }
    else if (dimensionIn == 2)
    {
        nodelist.clear();
        elementlist.clear();
        node node;
        volumeElement refElement;

        this->dimension = dimensionIn;

        nOrder = 1;
        nElements = 1;
        nPoints = 3;
        nBoundaryPoints = 3;
        nDirichlet = nBoundaryPoints;
        nNeumann = 0;
        nFaces = 3;
        nBoundaryFaces = 3;

        node.setNodeValues(0.0, 0.0, true);
        nodelist.push_back(node);

        node.setNodeValues(1.0, 0.0, true);
        nodelist.push_back(node);

        node.setNodeValues(0.0, 1.0, true);
        nodelist.push_back(node);

        refElement.setBasicElementStuff(std::vector<int>({0, 1, 2}));
        refElement.setFace(0,0);
        refElement.setFace(1,1);
        refElement.setFace(2,2);


        elementlist.push_back(refElement);

        facelist.resize(dimension + 1);
        facelist[0].fillFace(0, -1, std::vector<int>({0, 1}));
        facelist[1].fillFace(0, -1, std::vector<int>({1, 2}));
        facelist[2].fillFace(0, -1, std::vector<int>({2, 0}));
    }
    else if (dimensionIn == 3)
    {
        nodelist.clear();
        elementlist.clear();
        node node;
        volumeElement refElement;

        this->dimension = dimensionIn;

        nOrder = 1;
        nElements = 1;
        nPoints = 4;
        nBoundaryPoints = 4;
        nNeumann = 0;
        nFaces = 4;

        node.setNodeValues(0.0, 0.0, 0.0, true);
        nodelist.push_back(node);

        node.setNodeValues(1.0, 0.0, 0.0, true);
        nodelist.push_back(node);

        node.setNodeValues(0.0, 1.0, 0.0, true);
        nodelist.push_back(node);

        node.setNodeValues(0.0, 0.0, 1.0, true);
        nodelist.push_back(node);

        refElement.setBasicElementStuff(std::vector<int>({0, 1, 2, 3}));
        refElement.setFace(0,0);
        refElement.setFace(1,1);
        refElement.setFace(2,2);
        refElement.setFace(3,3);


        elementlist.push_back(refElement);

        facelist.resize(dimension + 1);
        facelist[0].fillFace(0, -1, std::vector<int>({0, 1, 3}));
        facelist[1].fillFace(0, -1, std::vector<int>({1, 2, 3}));
        facelist[2].fillFace(0, -1, std::vector<int>({2, 0, 3}));
        facelist[3].fillFace(0, -1, std::vector<int>({0, 1, 2}));
    }
    else
    {
        std::cerr << std::endl << "Reference element of dimension " << dimension << " is not implemented" << std::endl;
        exit(EXIT_FAILURE);
    }
}


void meshClass::defineRefElement_SecondOrder(int dimension)
{
    if (dimension==3){
        nodelist.clear();
        elementlist.clear();
        node node;
        volumeElement refElement;

        this->dimension = dimension;

        nOrder = 1;
        nElements = 1;
        nPoints = 10;
        nBoundaryPoints = 10;
        nNeumann = 0;
        nFaces = 4;

        //p1
        node.setNodeValues(0.0, 0.0, 0.0, true);
        nodelist.push_back(node);

        //p2
        node.setNodeValues(1.0, 0.0, 0.0, true);
        nodelist.push_back(node);

        //p3
        node.setNodeValues(0.0, 1.0, 0.0, true);
        nodelist.push_back(node);

        //p4
        node.setNodeValues(0.0, 0.0, 1.0, true);
        nodelist.push_back(node);

        refElement.setBasicElementStuff(std::vector<int>({0, 1, 2, 3}));
        refElement.setFace(0,0);
        refElement.setFace(1,1);
        refElement.setFace(2,2);
        refElement.setFace(3,3);


        elementlist.push_back(refElement);

        facelist.resize(dimension + 1);
        facelist[0].fillFace(0, -1, std::vector<int>({0, 1, 3}));
        facelist[1].fillFace(0, -1, std::vector<int>({1, 2, 3}));
        facelist[2].fillFace(0, -1, std::vector<int>({2, 0, 3}));
        facelist[3].fillFace(0, -1, std::vector<int>({0, 1, 2}));

        //p5
        node.setNodeValues(0.5, 0, 0, true);
        nodelist.push_back(node);

        //p6
        node.setNodeValues(0.5, 0.5 , 0, true);
        nodelist.push_back(node);

        //p7
        node.setNodeValues(0, 0.5 , 0, true);
        nodelist.push_back(node);

        //p8
        node.setNodeValues(0.0, 0, 0.5, true);
        nodelist.push_back(node);

        //p9
        node.setNodeValues(0.5, 0, 0.5, true);
        nodelist.push_back(node);

        //p10
        node.setNodeValues(0, 0.5 , 0.5, true);
        nodelist.push_back(node);

    }
}

void meshClass::defineRefElement_ThirdOrder(int dimension){
    if (dimension==3){
        nodelist.clear();
        elementlist.clear();
        node node;
        volumeElement refElement;

        this->dimension = dimension;

        nOrder = 1;
        nElements = 1;
        nPoints = 10;
        nBoundaryPoints = 10;
        nNeumann = 0;
        nFaces = 4;

        //p1
        node.setNodeValues(0.0, 0.0, 0.0, true);
        nodelist.push_back(node);

        //p2
        node.setNodeValues(1.0, 0.0, 0.0, true);
        nodelist.push_back(node);

        //p3
        node.setNodeValues(0.0, 1.0, 0.0, true);
        nodelist.push_back(node);

        //p4
        node.setNodeValues(0.0, 0.0, 1.0, true);
        nodelist.push_back(node);

        refElement.setBasicElementStuff(std::vector<int>({0, 1, 2, 3}));
        refElement.setFace(0,0);
        refElement.setFace(1,1);
        refElement.setFace(2,2);
        refElement.setFace(3,3);


        elementlist.push_back(refElement);

        facelist.resize(dimension + 1);
        facelist[0].fillFace(0, -1, std::vector<int>({0, 1, 3}));
        facelist[1].fillFace(0, -1, std::vector<int>({1, 2, 3}));
        facelist[2].fillFace(0, -1, std::vector<int>({2, 0, 3}));
        facelist[3].fillFace(0, -1, std::vector<int>({0, 1, 2}));

        //p5
        node.setNodeValues( 1.0/3.0 , 0, 0, true);
        nodelist.push_back(node);

        //p6
        node.setNodeValues( 2.0/3.0 , 0, 0, true);
        nodelist.push_back(node);

        //p7
        node.setNodeValues(2.0/3.0, 1.0/3.0 , 0, true);
        nodelist.push_back(node);

        //p8
        node.setNodeValues(1.0/3.0, 2.0/3.0 , 0, true);
        nodelist.push_back(node);

        //p9
        node.setNodeValues(0, 2.0/3.0, 0, true);
        nodelist.push_back(node);

        //p10
        node.setNodeValues(0, 1.0/3.0, 0, true);
        nodelist.push_back(node);

        //p11
        node.setNodeValues(0, 0 , 1.0/3.0, true);
        nodelist.push_back(node);

        //p12
        node.setNodeValues(0, 0 , 2.0/3.0, true);
        nodelist.push_back(node);

        //p13
        node.setNodeValues( 2.0/3.0 , 0, 1.0/3.0, true);
        nodelist.push_back(node);

        //p14
        node.setNodeValues( 1.0/3.0 , 0, 2.0/3.0, true);
        nodelist.push_back(node);

        //p15
        node.setNodeValues(0, 2.0/3.0 , 1.0/3.0, true);
        nodelist.push_back(node);

        //p16
        node.setNodeValues(0, 1.0/3.0 , 2.0/3.0, true);
        nodelist.push_back(node);

        //p17
        node.setNodeValues(1.0/3.0, 1.0/3.0, 0, true);
        nodelist.push_back(node);

        //p18
        node.setNodeValues(0, 1.0/3.0, 1.0/3.0, true);
        nodelist.push_back(node);

        //p19
        node.setNodeValues(1.0/3.0, 1.0/3.0 , 1.0/3.0, true);
        nodelist.push_back(node);

        //p20
        node.setNodeValues(0, 1.0/3.0 , 1.0/3.0, true);
        nodelist.push_back(node);

    }
}

void meshClass::createTestMesh(const int n){

    if (dimension == 1)
    {
        if (n < 2){
            std::cerr << "exception: " << std::domain_error("Es werden wenigstens 2 Punkte in jede Richtung gebraucht um ein Netz zu konstruieren. ").what() << std::endl;
            exit(EXIT_FAILURE);
        }


        dimension = 1;
        nOrder = 1;
        nPoints = n;
        nElements = n - 1;
        nFaces = n;
        nBoundaryFaces = 2;


        // Definiere Gebiet
        double x1=0;
        double x2=1;

        double h = (x2 - x1) / nElements;

        nodelist.resize(nPoints);
        elementlist.resize(nElements);
        facelist.resize(nFaces);


        //Elements: Indices of Points
        for (int x_ind = 0; x_ind < nPoints; x_ind++)
        {
            nodelist[x_ind].setX(x_ind * h);
            if ( CLOSE(x_ind * h, x1) || CLOSE(x_ind * h, x2) ){
                set_boundary(true, x_ind);
            }
        }


        for (int elementIndex = 0; elementIndex < nElements; elementIndex++)
        {
            elementlist[elementIndex].setBasicElementStuff(std::vector<int>({elementIndex, elementIndex + 1}));

            elementlist[elementIndex].setFace(0, elementIndex);
            elementlist[elementIndex].setFace(1, elementIndex + 1);


        }

        int boundaryTemp;

        boundaryTemp = facelist[0].fillFace(-1, 0, std::vector<int>({0}));

        for (int faceIndex = 1; faceIndex < nFaces-1; faceIndex++)
        {
            boundaryTemp = facelist[faceIndex].fillFace(faceIndex-1, faceIndex, std::vector<int>({faceIndex}));
        }

        boundaryTemp = facelist[nFaces-1].fillFace(nElements-1, -1, std::vector<int>({nPoints-1}));

        determineTransformations();

    }
    else if(dimension == 2)
    {
        if (n < 2){
            std::cerr << "exception: " << std::domain_error("Es werden wenigstens 2 Punkte in jede Richtung gebraucht um ein Netz zu konstruieren. ").what() << std::endl;
            exit(EXIT_FAILURE);
        }

        dimension = 2;
        nOrder = 1;
        nPoints = n*n;
        nElements = 2 * (n-1)*(n-1);
        nFaces = 3*nElements/2+(n-1)*2;

        double h = 1.0/(double)(n-1.0);

        nodelist.resize(nPoints);
        elementlist.resize(nElements);
        facelist.resize(nFaces);


        // Erstelle zunaechst die Pointlist
        int counter = 0;
        LOOP(y_ind,n){
            LOOP(x_ind,n){

                set_point(x_ind * h, y_ind * h, counter);
                if ( ISZERO(x_ind * h) || CLOSE(x_ind * h, 1.0) || ISZERO(y_ind * h) || CLOSE(y_ind * h, 1)){
                    set_boundary(true, counter);
                }

                counter++;
            }
        }


        int T1, T2, T3, T4, elementNumber;
        int nx, ny, e2, boundaryTemp;
        int faceNumber = 0;
        // Erstelle nun die Elementlist und facelist durch Durchlaufen aller Vierecke
        LOOP(yt_ind,n-1){
            LOOP(xt_ind,n-1){
                // Eckpunkte des ersten Quadrats
                T1 = yt_ind * n + xt_ind + 1 -1;
                T2 = yt_ind * n + xt_ind + 2 -1;
                T3 = (yt_ind + 1) * n + xt_ind + 1;
                T4 = (yt_ind + 1) * n + xt_ind + 1 -1;

                // erstes Element
                elementNumber = yt_ind * (n-1) * 2 + xt_ind * 2 ;
                elementlist[elementNumber].setBasicElementStuff(std::vector<int>({T1, T2, T3}));

                // zweites Element
                elementlist[elementNumber+1].setBasicElementStuff(std::vector<int>({T1, T3, T4}));

                // first face first element
                nx = xt_ind*2+1;
                ny = faceCheckY(nElements-1, (yt_ind-1)*(n-1)*2);

                if (ny == -1)
                {
                    boundaryTemp = facelist[faceNumber].fillFace(elementNumber, -1, std::vector<int>({T1, T2}));

                    nBoundaryFaces++;

                    elementlist[elementNumber].setFace(0, faceNumber);
                    faceNumber++;
                }
                else
                {
                    e2 = nx + ny;
                    if (e2 > elementNumber){
                        boundaryTemp = facelist[faceNumber].fillFace(elementNumber, e2, std::vector<int>({T1, T2}));

                        elementlist[elementNumber].setFace(0, faceNumber);
                        elementlist[e2].setFace(1, faceNumber);
                        faceNumber++;
                    }
                }
                //
                // second face first element
                nx = faceCheckX(2*(n-1)-1, (xt_ind + 1)*2+1);
                ny = yt_ind*(n-1)*2;



                if (nx == -1)
                {
                    boundaryTemp = facelist[faceNumber].fillFace(elementNumber, -1, std::vector<int>({T2, T3}));

                    nBoundaryFaces++;

                    elementlist[elementNumber].setFace(1, faceNumber);
                    faceNumber++;
                }
                else
                {
                    e2 = nx + ny;
                    if (e2 > elementNumber){
                        boundaryTemp = facelist[faceNumber].fillFace(elementNumber, e2, std::vector<int>({T2, T3}));

                        elementlist[elementNumber].setFace(1, faceNumber);
                        elementlist[e2].setFace(2, faceNumber);
                        faceNumber++;
                    }
                }

                // third face first element; first face second element
                if(facelist[faceNumber].fillFace(elementNumber, elementNumber+1, std::vector<int>({T3, T1})))
                    nBoundaryFaces++;
                elementlist[elementNumber].setFace(2, faceNumber);
                elementlist[elementNumber+1].setFace(0, faceNumber);
                faceNumber++;


                // second face second element
                nx = xt_ind*2;
                ny = faceCheckY(nElements-1, (yt_ind+1)*(n-1)*2);

                if (ny == -1)
                {
                    boundaryTemp = facelist[faceNumber].fillFace(elementNumber+1, -1, std::vector<int>({T3, T4}));

                    nBoundaryFaces++;

                    elementlist[elementNumber+1].setFace(1, faceNumber);
                    faceNumber++;
                }
                else
                {
                    e2 = nx + ny;
                    if (e2 > elementNumber){
                        boundaryTemp = facelist[faceNumber].fillFace(elementNumber+1, e2, std::vector<int>({T3, T4}));

                        elementlist[elementNumber+1].setFace(1, faceNumber);
                        elementlist[e2].setFace(0, faceNumber);
                        faceNumber++;
                    }
                }


                // third face second element
                nx = faceCheckX(2*(n-1)-1, xt_ind*2-2);
                ny = yt_ind*(n-1)*2;

                if (nx == -1)
                {
                    boundaryTemp = facelist[faceNumber].fillFace(elementNumber+1, -1, std::vector<int>({T4, T1}));

                    nBoundaryFaces++;

                    elementlist[elementNumber+1].setFace(2, faceNumber);
                    faceNumber++;
                }
                else
                {
                    e2 = nx + ny;
                    if (e2 > elementNumber){
                        boundaryTemp = facelist[faceNumber].fillFace(elementNumber+1, e2, std::vector<int>({T4, T1}));

                        elementlist[elementNumber+1].setFace(2, faceNumber);
                        elementlist[e2].setFace(1, faceNumber);
                        faceNumber++;
                    }
                }

            }
        }


        LOG(output) << "FINISHED CREATING STRUCTURED TEST MESH (n = " << n << ")" ;


        determineTransformations();

    }
    else if(dimension == 3)
    {
        if (n < 2){
            std::cerr << "exception: " << std::domain_error("Es werden wenigstens 2 Punkte in jede Richtung gebraucht um ein Netz zu konstruieren. ").what() << std::endl;
            exit(EXIT_FAILURE);
        }

        int nCubes = n - 1;

        nOrder = 1;
        build_completeMesh(nCubes);

        determineTransformations();
    }
    else
    {
        std::invalid_argument e("Building testmesh is only implemented for dimension 1, 2 and 3 ");
        std::cerr << "exception: " << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}


int meshClass::faceCheckX(int upperLimit, int x){

    if (x < 0 || x > upperLimit)
        return -1;
    return x;
}

int meshClass::faceCheckY(int upperLimit, int x){

    if (x < 0 || x > upperLimit)
        return -1;
    return x;
}

int meshClass::getNeighbour(int elIndex, int localFaceIndex)
{
    return facelist[elementlist[elIndex].getFaceIndex(localFaceIndex)].getNeighbourFromElement(elIndex);
}

void meshClass::determineTransformations()
{
    element *el;
    std::vector<point*> points;
    LOOP(elementIndex, nElements){

        el = &elementlist[elementIndex];
        points = getVecOfPointsFromElement(el);
        points.resize(dimension + 1);
        el->setTransStuff(points);
    }

}

std::vector<point*> meshClass::getVecOfPointsFromElement(element *el)
{
    std::vector<point*> points;

    LOOP(nodeIndex, el->getElementSize()){

        points.push_back(&nodelist[el->getNodeIndex(nodeIndex)]);
    }

    return points;
}

node computeLinNode(point *p0, point *p1, double affine, bool boundary)
{
    node pRet = *p0 + (*p1 - *p0) * affine;
    pRet.setDirichlet(boundary);

    return pRet;
}

void meshClass::addHighOrderPointToElement(int elementIndex, int nodeIndex, int faceIndex)
{
    if(elementIndex == -1)
        return;

    volumeElement *el = &elementlist[elementIndex];
    LOOP(i, dimension+1){

        if (el->getFaceIndex(i) == faceIndex)
            el->setNodeLukas(dimension + 1 + i, nodeIndex );

    }
}

void meshClass::addHighOrderPoints(int order)
{

    if (dimension == 1)
    {
        if (order == 1)
        {

        }
        else if (order == 2)
        {
            nOrder = order;

            for (auto &it : elementlist)
                it.allocateNodelist(3);

            point *p0,*p1;
            int nodeIndex;
            volumeElement *el;

            LOOP(elementIndex, nElements)
            {
                el = &elementlist[elementIndex];
                p0 = &nodelist[el->getNodeIndex(0)];
                p1 = &nodelist[el->getNodeIndex(1)];

                nodeIndex = insertNode(computeLinNode(p0, p1, 0.5, 0));
                el->setNodeLukas(2, nodeIndex);
            }
        }
        else if (order == 3)
        {
            nOrder = order;

            for (auto &it : elementlist)
                it.allocateNodelist(4);

            point *p0,*p1;
            int nodeIndex;
            volumeElement element;

            LOOP(elementIndex, nElements)
            {
                element = elementlist[elementIndex];
                p0 = &nodelist[element.getNodeIndex(0)];
                p1 = &nodelist[element.getNodeIndex(1)];

                nodeIndex = insertNode(computeLinNode(p0, p1, 1.0/3.0, 0));
                element.setNodeLukas(2, nodeIndex);

                nodeIndex = insertNode(computeLinNode(p0, p1, 2.0/3.0, 0));
                element.setNodeLukas(3, nodeIndex);
            }
        }
        else
        {
            std::invalid_argument e("addHighOrderPoints is implemented only for order 1, 2 and 3 ");
            std::cerr << "exception: " << e.what() << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    if (dimension == 2)
    {
        if (order == 1)
        {

        }
        else if (order == 2)
        {
            nOrder = order;

            for (auto &it : elementlist)
                it.allocateNodelist(6);

            point *p0,*p1;
            int nodeIndex;
            faceElement face;

            LOOP(faceIndex, nFaces)
            {
                face = facelist[faceIndex];
                p0 = &nodelist[face.getNodeIndex(0)];
                p1 = &nodelist[face.getNodeIndex(1)];

                nodeIndex = insertNode(computeLinNode(p0, p1, 0.5, face.isBoundary()));
                addHighOrderPointToElement(face.getNeighbour(0), nodeIndex, faceIndex);
                addHighOrderPointToElement(face.getNeighbour(1), nodeIndex, faceIndex);
            }
        }
        else if (order == 3)
        {
        }
        else
        {
            std::invalid_argument e("addHighOrderPoints is implemented only for order 1, 2 and 3 ");
            std::cerr << "exception: " << e.what() << std::endl;
            exit(EXIT_FAILURE);
        }
    }if (dimension == 3)
    {
        if (order == 1)
        {

        }
        else if (order == 2)
        {
            nOrder = 2;

            generate_allhighOrderPoints(order, (int)(pow(nPoints,1.0/3.0)-0.9));
        }
        else if (order == 3)
        {
        }
        else
        {
            std::invalid_argument e("addHighOrderPoints is implemented only for order 1, 2 and 3 ");
            std::cerr << "exception: " << e.what() << std::endl;
            exit(EXIT_FAILURE);
        }
    }

}



void meshClass::build_completeMesh(int n){
    set_tetrahedralVertices(n);
    build_allTetrahedrons(n);
    build_all_Faces(n);
}



void meshClass::generate_allhighOrderPoints(int order, int n){
    prepareElementsforHighOrderPoints(order);
    LOOP(l,n)
    LOOP(j,n)
    LOOP(i,n)
    for (int t=1; t<5;t++)
        addHighOrderPoints(order, n ,i,j,l,t);

    LOOP(l,n)
    LOOP(j,n)
    LOOP(i,n)
    generate_special_highOrderPoints(order, n,  i, j,  l);

}

