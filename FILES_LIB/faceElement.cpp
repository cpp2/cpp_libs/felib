
#include "./faceElement.hpp"


int faceElement::fillFace(int n1, int n2, std::vector<int> nodelist)
{
    neighbour1 = n1;
    neighbour2 = n2;

    this->nodelist = nodelist;

    return isBoundary();
}

int faceElement::getNeighbour1(){
    return neighbour1;
}
int faceElement::getNeighbour2(){
    return neighbour2;
}
void faceElement::setNeighbour1(int T)
{
    neighbour1=T;
}

void faceElement::setNeighbour2(int T)
{
    neighbour2=T;
}

int faceElement::getNeighbour(int neighbour) const{
    int neighbour_out = 0 ;

    if (neighbour == 0) neighbour_out =  neighbour1 ;
    if (neighbour == 1) neighbour_out =  neighbour2 ;

    return neighbour_out ;
}

int faceElement::getNeighbourFromElement(int element)
{
    if (element == neighbour1) return neighbour2;
    else if (element == neighbour2) return neighbour1;

    return -5;
}

int faceElement::isBoundary()
{
    if (neighbour1 == -1 || neighbour2 == -1)
        return -1;
    if (neighbour1 == -2 || neighbour2 == -2)
        return -2;
    return 0;
}

bool faceElement::operator==(const faceElement &fIn)
    {
    if (neighbour1 == fIn.getNeighbour(0) && neighbour2 == fIn.getNeighbour(1))
        return true;
    return false;
    }

std::ostream & operator<< (std::ostream & out, const faceElement & face)
{
    out << (element)face;
    out << " \t" << "Nachbarn: " << face.getNeighbour(0) << ", " << face.getNeighbour(1);
    return out;
}
