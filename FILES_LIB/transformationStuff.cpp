#include "./transformationStuff.hpp"


transformationStuff::transformationStuff()
{}

transformationStuff::transformationStuff(int dim):B_T(denseMatrix2(dim)), BTB(denseMatrix2(dim))
{}

transformationStuff::~transformationStuff()
{}


denseMatrix2 * transformationStuff::getB_T() {return &B_T ;}

denseMatrix2 * transformationStuff::getBTB() {return &BTB ;}

point * transformationStuff::getb_affin() {return &b_affin ;}

void transformationStuff::transform_back(point &xGlobal, point &xRef)
{
    denseMatrix2 B1 = B_T.getInverse();

    point p = xGlobal;
    p = p - b_affin;

    xRef = B1 * p;
}

void transformationStuff::transform(const point &xRef, point &xGlobal)
{
    xGlobal = B_T * xRef;
    xGlobal = xGlobal + b_affin;
}

void transformationStuff::fill(std::vector<point*> points)
{
    if (points.size()==2)
    {
        B_T = denseMatrix2(1);
        BTB = denseMatrix2(1);
        B_T.setTransposed(false);
        BTB.setTransposed(false);

        // Transformationsmatrix B_T ist einfach die Elementlänge h in 1D
        B_T.setEntry(0, 0, points[1]->getX() - points[0]->getX() );

        // Zeige mit b_affin auf den Punkt, von dem aus B_T Punkte verschiebt
        b_affin = *points[0] ;
        // Berechne noch die Determinante der Transformation
        B_T.computeDet() ;

        compBTB();
    }
    else if (points.size()==3)
    {
        double x0, x1, x2, y0, y1, y2 ;
        point *p1 = points.at(0);
        point *p2 = points.at(1);
        point *p3 = points.at(2);

        B_T = denseMatrix2(2);
        BTB = denseMatrix2(2);
        B_T.setTransposed(false);
        //    BTB->setTransposed(false);

        // Lese Knotenkoordinaten des Dreiecks aus
        x0 = p1->getX() ;
        y0 = p1->getY() ;

        x1 = p2->getX() ;
        y1 = p2->getY() ;

        x2 = p3->getX() ;
        y2 = p3->getY() ;

        // Baue Transformationsmatrix B_T
        // B = [x1-x0, x2-x0; y1-y0, y2-y0]
        B_T.setEntry(0, 0, x1 - x0) ;
        B_T.setEntry(1, 0, y1 - y0) ;
        B_T.setEntry(0, 1, x2 - x0) ;
        B_T.setEntry(1, 1, y2 - y0) ;

        // Zeige mit b_affin auf den Punkt, von dem aus B_T Punkte verschiebt
        b_affin = *p1 ;
        // Berechne noch die Determinante der Transformation
        B_T.computeDet() ;

        compBTB();
    }
    else if (points.size() == 4)
    {
        double x0, x1, x2, x3, y0, y1, y2, y3, z0, z1, z2, z3 ;

        point *p1 = points.at(0);
        point *p2 = points.at(1);
        point *p3 = points.at(2);
        point *p4 = points.at(3);

        B_T = denseMatrix2(3);
        BTB = denseMatrix2(3);
        B_T.setTransposed(false);
        //    BTB->setTransposed(false);

        // Lese Knotenkoordinaten des Tetraeders aus
        x0 = p1->getX() ; // Erster Punkt
        y0 = p1->getY() ;
        z0 = p1->getZ() ;

        x1 = p2->getX() ; // Zweiter Punkt
        y1 = p2->getY() ;
        z1 = p2->getZ() ;

        x2 = p3->getX() ; // Dritter Punkt
        y2 = p3->getY() ;
        z2 = p3->getZ() ;

        x3 = p4->getX() ; // Vierter Punkt
        y3 = p4->getY() ;
        z3 = p4->getZ() ;

        // Baue Transformationsmatrix B_T
        // B = [x1-x0, x2-x0, x3-x0 ; y1-y0, y2-y0, y3-y0; z1-z0, z2-z0, z3-z0]
        // Erste Spalte
        B_T.setEntry(0, 0, x1-x0 ) ;
        B_T.setEntry(1, 0, y1-y0 ) ;
        B_T.setEntry(2, 0, z1-z0 ) ;
        // Zweite Spalte
        B_T.setEntry(0, 1, x2-x0 ) ;
        B_T.setEntry(1, 1, y2-y0 ) ;
        B_T.setEntry(2, 1, z2-z0 ) ;
        // Dritte Spalte
        B_T.setEntry(0, 2, x3-x0 ) ;
        B_T.setEntry(1, 2, y3-y0 ) ;
        B_T.setEntry(2, 2, z3-z0 ) ;

        denseMatrix2 B_T_inv = B_T.getInverse();
        denseMatrix2 B_T_invT = B_T_inv;
        B_T_invT.transpose();

        BTB = B_T_inv * B_T_invT;

        BTB.transpose();

        // Alternativ HART
        //        compBTB() ;

        // Zeige mit b_affin auf den Punkt, von dem aus B_T Punkte verschiebt
        b_affin = *p1 ;
        // Berechne noch die Determinante der Transformation
        B_T.computeDet() ;
    }

}

void transformationStuff::compBTB(){

    if (B_T.getRows() == 1)
    {
        BTB.setEntry(0, 0, 1.0 / (B_T.getDet() * B_T.getDet()));
    }
    else if (B_T.getRows() == 2)
    {
        double a11, a22, a21, a12, det ;

        a11 = B_T.getEntry(0, 0) ;
        a12 = B_T.getEntry(0, 1) ;
        a21 = B_T.getEntry(1, 0) ;
        a22 = B_T.getEntry(1, 1) ;

        det = B_T.getDet() ;

        //	cout << a11 << a12 << a21 << a22 << endl ;

        double b11, b12, b21, b22, factor ;

        factor = 1 / (det * det) ;
        b22 = factor * (a22*a22 + a21*a21) ;
        b12 = factor * (-a22*a12 + a21*a21) ;
        b21 = factor * (-a12*a22 - a11*a21) ;
        b11 = factor * (a12*a12 + a11*a11) ;
        BTB.setEntry(0, 0, b11) ;
        BTB.setEntry(0, 1, b12) ;
        BTB.setEntry(1, 0, b21) ;
        BTB.setEntry(1, 1, b22) ;
    }
    else if (B_T.getRows() == 3)
    {
        double a11, a12, a13, a21, a22, a23, a31, a32, a33, det ;

        a11 = B_T.getEntry(0, 0) ;
        a12 = B_T.getEntry(0, 1) ;
        a13 = B_T.getEntry(0, 2) ;
        a21 = B_T.getEntry(1, 0) ;
        a22 = B_T.getEntry(1, 1) ;
        a23 = B_T.getEntry(1, 2) ;
        a31 = B_T.getEntry(2, 0) ;
        a32 = B_T.getEntry(2, 1) ;
        a33 = B_T.getEntry(2, 2) ;

        B_T.computeDet() ;
        det = B_T.getDet();

        double b11, b12, b13, b21, b22, b23, b31, b32, b33, factor ;

        factor = 1/(det*det) ;

        b11 = factor * ( (a21*a32 - a22*a31)*(a21*a32 - a22*a31)
            + (a21*a33 - a23*a31)*(a21*a33 - a23*a31)
            + (a22*a33 - a23*a32)*(a22*a33 - a23*a32) ) ;
        b12 = factor * ( - (a11*a32 - a12*a31)*(a21*a32 - a22*a31)
            - (a11*a33 - a13*a31)*(a21*a33 - a23*a31)
            - (a12*a33 - a13*a32)*(a22*a33 - a23*a32) ) ;
        b13 = factor * ( (a11*a22 - a12*a21)*(a21*a32 - a22*a31)
            + (a11*a23 - a13*a21)*(a21*a33 - a23*a31)
            + (a12*a23 - a13*a22)*(a22*a33 - a23*a32) ) ;

        b21 = factor * ( - (a11*a32 - a12*a31)*(a21*a32 - a22*a31)
            - (a11*a33 - a13*a31)*(a21*a33 - a23*a31)
            - (a12*a33 - a13*a32)*(a22*a33 - a23*a32) ) ;
        b22 = factor * ( (a11*a32 - a12*a31)*(a11*a32 - a12*a31)
            + (a11*a33 - a13*a31)*(a11*a33 - a13*a31)
            + (a12*a33 - a13*a32)*(a12*a33 - a13*a32) ) ;
        b23 = factor * ( - (a11*a22 - a12*a21)*(a11*a32 - a12*a31)
            - (a11*a23 - a13*a21)*(a11*a33 - a13*a31)
            - (a12*a23 - a13*a22)*(a12*a33 - a13*a32) ) ;

        b31 = factor * ( (a11*a22 - a12*a21)*(a21*a32 - a22*a31)
            + (a11*a23 - a13*a21)*(a21*a33 - a23*a31)
            + (a12*a23 - a13*a22)*(a22*a33 - a23*a32) ) ;
        b32 = factor * ( - (a11*a22 - a12*a21)*(a11*a32 - a12*a31)
            - (a11*a23 - a13*a21)*(a11*a33 - a13*a31)
            - (a12*a23 - a13*a22)*(a12*a33 - a13*a32) ) ;
        b33 = factor * ( (a11*a22 - a12*a21)*(a11*a22 - a12*a21)
            + (a11*a23 - a13*a21)*(a11*a23 - a13*a21)
            + (a12*a23 - a13*a22)*(a12*a23 - a13*a22) ) ;

        BTB.setEntry(0,0,b11) ;
        BTB.setEntry(0,1,b12) ;
        BTB.setEntry(0,2,b13) ;
        BTB.setEntry(1,0,b21) ;
        BTB.setEntry(1,1,b22) ;
        BTB.setEntry(1,2,b23) ;
        BTB.setEntry(2,0,b31) ;
        BTB.setEntry(2,1,b32) ;
        BTB.setEntry(2,2,b33) ;
    }

}



