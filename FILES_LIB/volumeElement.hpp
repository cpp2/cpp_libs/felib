/*!
 * \file volumeElement.hpp
 * \brief Klasse für Volumenelemente, abgeleitet von element
 */
#pragma once

#include <vector>

#include "./element.hpp"

/*!
 * Klasse zur Speicherung der Tetraeder
 * jedes Tetraeder speichert die Indexnummern seiner Faces in der Facelist des Gitters
 */
class volumeElement : public element{
protected:
    std::vector<int> faces;

public:
    /// Konstruktoren
    volumeElement();
    /// Konstruktoren
    volumeElement(int T1, int T2, int T3, int T4);

    /*!
     * fuegt ein Face in die Facelist ein
     */
    void insertFace(int T);
/*!
 * print-Funktion
 */
    void print();
    /*!
     * Setter fuer die Faces
     */
    void setFace(int index, int f);
    /*!
     * Getter fuer i-ten Index in der Facelist
     */
    int getFaceIndex(int index) const ;
    /*!
     * gibt Anzahl der Faces wieder
     */
    int getNfaces() const ;

    ///Überladen des << Operators, so dass die Faces geprintet werden
    friend std::ostream & operator<< (std::ostream & out, const volumeElement & elem) ;

} ;



