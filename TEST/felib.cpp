#include "gtest/gtest.h"

#include "myLib_wrapper.hpp"
#include "matrixLib_wrapper.hpp"
#include "feLib_wrapper.hpp"


void fe_test()
{
    auto log_options = myLib::getStandardLoggerOptions();
    myLib::initLogger(log_options);

    const int DIMENSION = 2;

    int n=5;

    int order = 2;

    LOG(trace) << "Start program";

    //    Aufrufen des Objektes
    meshClass mesh;

    mesh.setDimension(DIMENSION);

//    mesh.build_completeMesh(n);

//    mesh.generate_allhighOrderPoints(2, n);

    mesh.createTestMesh(n);

    mesh.addHighOrderPoints(2);

    //  mesh.defineRefElement_SecondOrder(3);

    mesh.print();

}

TEST(matrixlibtests, determinante)
{
    std::cout << "test" << std::endl;
    EXPECT_NO_THROW(fe_test());
}
