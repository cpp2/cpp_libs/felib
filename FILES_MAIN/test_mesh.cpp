#include <stdlib.h>
#include <stdio.h>

#include "myLib_wrapper.hpp"

#include "matrixLib_wrapper.hpp"

#include "feLib_wrapper.hpp"



int main (int argc, char* argv[])
{
    const int DIMENSION = 2;

    int n=5;

    int order = 2;

    auto vm = myLib::parseCommandlineArguments(argc, argv);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);

    LOG(trace) << "Start program";

    //    Aufrufen des Objektes
    meshClass mesh;

    mesh.setDimension(DIMENSION);

//    mesh.build_completeMesh(n);

//    mesh.generate_allhighOrderPoints(2, n);

    mesh.createTestMesh(n);

    mesh.addHighOrderPoints(2);

    //  mesh.defineRefElement_SecondOrder(3);

    mesh.print();

    return 0;
}
